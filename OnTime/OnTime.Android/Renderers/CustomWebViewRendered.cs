﻿using Ace;
using Android.Webkit;

using OnTime.Droid.Renderers;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Xamarin.Forms.WebView), typeof(CustomWebViewRenderer))]

namespace OnTime.Droid.Renderers
{
    public class GeoWebChromeClient : WebChromeClient
    {
        public override async void OnGeolocationPermissionsShowPrompt(string origin, GeolocationPermissions.ICallback callback)
        {
            await Aides.GetActiveLocationOrDefaultBySystem();
            callback.Invoke(origin, true, true);
        }
	}

    public class CustomWebViewRenderer : WebViewRenderer
    {
        public CustomWebViewRenderer(Android.Content.Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.WebView> e)
        {
            base.OnElementChanged(e);

            Control.SetWebChromeClient(new GeoWebChromeClient());
            Control.Settings.To(out var s);
            {
                //s.UserAgentString = "Mozilla/5.0 (Linux; Android 5.0)";
                s.JavaScriptCanOpenWindowsAutomatically = true;
                s.JavaScriptEnabled = true;
                s.SetGeolocationEnabled(true);
            }
        }
    }
}