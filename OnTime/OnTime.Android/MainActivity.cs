﻿using Ace;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Util;
using Android.Gms.Ads;

using System;
using System.Threading;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Xamarin.Forms.Platform.Android;

using Yandex.Metrica;
using Android.Webkit;

namespace OnTime.Droid
{
	[Activity(
		Label = "в🕑время",
		Icon = "@drawable/Logo",
		Theme = "@style/MainTheme",
		MainLauncher = false,
		ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation
		)]
	public class MainActivity : FormsAppCompatActivity 
	{
		protected override async void OnCreate(Bundle savedInstanceState)
		{

			try
			{
				RateStatus.AppStoreLink = "https://play.google.com/store/apps/details?id=" + Xamarin.Essentials.AppInfo.PackageName;

				//Aides.HttpClientHandlerProvider = () => new SslClientHandler();
				Aides.GetCookies += url => CookieManager.Instance.GetCookie(url);
				Aides.SetCookies += (url, value) => CookieManager.Instance.SetCookie(url, value);
				Aides.SystemFontFamiliesProvider = LoadFonts;

				base.OnCreate(savedInstanceState);

				LoadApplication(new App());

				ThreadPool.QueueUserWorkItem(o => MobileAds.Initialize(ApplicationContext, App.AdMobAppId));
			}
			catch (Exception exception)
			{
				Log.Error("Loading Error", exception.ToString());
				for (var e = exception; e.Is(); e = e.InnerException)
					YandexMetrica.ReportError(e.Message, e);

				await this.ShowAlertDialogAsync
				(
					"Oops".Localize() + "!",
					exception.Message,
					neutral: "Ok".Localize()
				);
			}
		}

		[Obsolete]
		public override void OnBackPressed()
		{
			if (App.RootView.Is(out var rootView) && rootView.ActiveOverlay.Is(out var activeOverlay))
			{
				rootView.HideOverlay(activeOverlay);
			}
			else
			{
				var mainIntent = new Intent(Intent.ActionMain);
				mainIntent.AddCategory(Intent.CategoryHome);
				mainIntent.SetFlags(ActivityFlags.NewTask);
				StartActivity(mainIntent);
			}
		}

		static string[] LoadFonts() => LoadSystemFonts().Is(out var fonts) && fonts.Length > 0
			? fonts
			: new[] { "casual", "cursive", "monospace", "sans-serif", "serif", "serif-monospace" };

		static string[] LoadSystemFonts() => new[] { "/system/etc/fonts.xml", "/system/etc/system_fonts.xml" }
			.Where(File.Exists)
			.Select(ReadFontFamilies)
			.SelectMany()
			.Distinct()
			.OrderBy()
			.ToArray()
			;

		static readonly Regex FontFamilyRegex = @"<family name=.(?<key>.*?).>".ToRegex();
		static List<string> ReadFontFamilies(string fontsFileName)
		{
			try { return FontFamilyRegex.Matches(File.ReadAllText(fontsFileName)).Select(m => m.Groups["key"].Value).ToList(); }
			catch { return new(); }
		}
	}
}