﻿namespace OnTime.Converters
{
	class PageToWebMapConverter : Ace.Markup.Patterns.AValueConverter
	{
		public override object Convert(object value) => GeoMaps.GetActiveUri();
		//$"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=10\"/><iframe src=\"{GeoMaps.GetActiveUri()}\"></iframe>";
		//$"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=10\"/><script type=\"text/javascript\" charset=\"utf-8\" async src=\"{GeoMaps.GetActiveUri()}\"></script>";
	}
}