﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;

using Yandex.Metrica;

namespace OnTime
{
	public partial class App : Application
	{
		public static event EventHandler UnhandledException = (sender, args) =>
		{
			MessageBox.Show(args.ToString());
		};

		static App()
		{
			TaskScheduler.UnobservedTaskException += (sender, args) => UnhandledException(sender, args);
			AppDomain.CurrentDomain.UnhandledException += (sender, args) => UnhandledException(sender, args);

			var folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
			var metricaFolderPath = Path.Combine(folder, "OnTime");

			YandexMetricaFolder.SetCurrent(metricaFolderPath);
			YandexMetrica.Activate(AnaliticsKey);
		}

		public static AppView RootView;

		private void Application_Exit(object sender, ExitEventArgs e)
		{
			Ace.Store.Snapshot();
		}
	}
}
