﻿using System;
using System.Globalization;
#if WPF
using System.Windows.Data;
#else
using Xamarin.Forms;
#endif

namespace Timely.Converters
{
	class PageToWebMapConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
			"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=10\"/><iframe src=\"https://yandex.ru/map-widget/v1/?um=constructor%3Ad9d2081eb6b51533ad88bc6f5deb49de2e96b356ff93581fa61a89fcc057354d&amp;source=constructor\" width=\"500\" height=\"400\" frameborder=\"0\"></iframe>";
		//"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=10\"/><script type=\"text/javascript\" charset=\"utf-8\" async src=\"https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ad9d2081eb6b51533ad88bc6f5deb49de2e96b356ff93581fa61a89fcc057354d&amp;width=500&amp;height=400&amp;lang=ru_RU&amp;scroll=true\"></script>";
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
