﻿using Ace.Markup.Patterns;

using Xamarin.Forms;

namespace OnTime.Converters
{
	public class StringToColorConverter : AValueConverter
	{
		public override object Convert(object value) => Color.FromHex(value?.ToString());
	}
}
