﻿using Ace;
using Ace.Markup.Patterns;

using System;

namespace OnTime.Converters
{
	class DateTimeToStringConverter : AValueConverter.Reflected
	{
		readonly AppViewModel appViewModel = Store.Get<AppViewModel>();

		public override object Convert(object value, object parameter) => value.Is(out DateTime dateTime)
			? dateTime.ToString(parameter.To<string>(), appViewModel.ActiveCulture)
			: value;
	}
}
