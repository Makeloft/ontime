﻿using Ace;
using Ace.Markup.Patterns;

using System;
using System.Collections.Generic;
using System.Text;

namespace OnTime.Converters
{
	public static class ABC
	{
		public enum Keys
		{
			Cyrillic, Latin
		}

		private static readonly Dictionary<Keys, string> KeyToUppercase = new Dictionary<Keys, string>().Merge
		(
			"А Б В Г Д Е Ё Ж З И Й К Л М Н О П Р С Т У Ў Ф Х Ц Ч Ш Щ Ъ Ы Ь Э Ю Я".By(Keys.Cyrillic),
			"A B V G D E Ë Ž Z I Y K L M N O P R S T U Ŭ F H C Č Š Ŝ \" Y ' È Û Â".By(Keys.Latin)
			//"A B V G D E Yo Zh Z I Y K L M N O P R S T U U F Kh Ts Ch Sh Shch \" Y ' E Yu Ya"
		);

		public static (string[] Upper, string[] Lower) GetInBothCases(this Keys key) => (key.GetUppercase(), key.GetLowercase());
		public static string[] GetLowercase(this Keys key) => KeyToUppercase[key].ToLower().Split();
		public static string[] GetUppercase(this Keys key) => KeyToUppercase[key].Split();
	}

	class TranslitConverter : AValueConverter.Reflected
	{
		public ABC.Keys SourceKey { set => _sourceSymbols = value.GetInBothCases(); }
		public ABC.Keys TargetKey { set => _targetSymbols = value.GetInBothCases(); }

		(string[] Upper, string[] Lower) _sourceSymbols;
		(string[] Upper, string[] Lower) _targetSymbols;

		readonly StringBuilder builder = new();
		readonly AppViewModel appViewModel = Store.Get<AppViewModel>();

		public override object Convert(object value)
		{
			if (appViewModel.UseTransliteration.IsFalse() || value.IsNot()) return value;

			var text = (string)value;

			for (var i = 0; i < text.Length; i++)
			{
				var charValue = text[i];
				var sourceSymbol = text.Substring(i, 1);
				var sourceSymbols = char.IsLower(charValue) ? _sourceSymbols.Lower : _sourceSymbols.Upper;
				var targetSymbols = char.IsLower(charValue) ? _targetSymbols.Lower : _targetSymbols.Upper;
				var translitIndex = Array.IndexOf(sourceSymbols, sourceSymbol);
				var targetSymbol = translitIndex < 0 ? sourceSymbol : targetSymbols[translitIndex];

				builder.Append(targetSymbol);
			}

			var convertedValue = builder.ToString();
			builder.Clear();
			return convertedValue;
		}
	}
}
