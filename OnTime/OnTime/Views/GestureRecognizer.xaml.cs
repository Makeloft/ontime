﻿using System;
using Xamarin.Forms;
using OnTime.Extensions;
using static Xamarin.Forms.SwipeDirection;

namespace OnTime.Views
{
	public partial class GestureRecognizer
	{
		public GestureRecognizer() => InitializeComponent();

		public Picker Picker { get; set; }

		private void Left_Tapped(object sender, EventArgs e)	=> Picker?.ShiftSelection(-1);

		private void Right_Tapped(object sender, EventArgs e)	=> Picker?.ShiftSelection(+1);

		private void Swiped(object sender, SwipedEventArgs e)	=> Picker?.ShiftSelection(GetStep(e.Direction));

		private static int GetStep(SwipeDirection direction) => direction switch
		{
			Right or Down => +1,
			Left or Up => -1,
			_ => 0,
		};
	}
}
