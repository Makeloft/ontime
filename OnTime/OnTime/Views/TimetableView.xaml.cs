﻿using Ace;
using Ace.Controls;
using Ace.Markup;
using Ace.Markup.Patterns;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using OnTime.Extensions;
using OnTime.ViewModels;

namespace OnTime.Views
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class TimetableView
	{
		public class Event
		{
			public string Key { get; set; }
			public DateTime Value { get; set; }
		}

		public TimetableView()
		{
			InitializeComponent();
			ActiveDate = GetActiveDate();
			TimetableListView.ItemSelected += (o, e) => TimetableListView.TryScrollTo(e.SelectedItem);

			var appViewModel = Ace.Store.Get<AppViewModel>();
			var timetableViewModel = Ace.Store.Get<TimetableViewModel>();

			timetableViewModel[() => timetableViewModel.Root].Changed += args =>
				DayLabel?.ResetValue(BindingContextProperty);

			_clock = new Clock();
			_clock.MinuteChime += async () =>
			{
				var activeStop = timetableViewModel.Root?.ActiveItem?.ActiveItem?.ActiveItem?.ActiveItem?.ActiveItem;
				if (activeStop.IsNot())
					return;

				if (_upcoming.Event.Is(out var e) && e.Value < Clock.GetActiveDateTime())
				{
					_autoRefresh = activeStop.ForceActiveItemRefresh();
					TimetableListView.ResetValue(ListView.SelectedItemProperty);
				}

				var (oldDate, newDate) = (ActiveDate, GetActiveDate());
				if (oldDate.Day.IsNot(newDate.Day))
				{
					ActiveDate = newDate;
					DayLabel?.ResetValue(BindingContextProperty);
					await activeStop.Load();
				}
			};
		}

		private bool _autoRefresh;

		private static DateTime GetActiveDate() =>
			Clock.GetActiveDateTime().AddHours(-Clock.ActiveDayOffsetHours).Date;

		public DateTime ActiveDate { get; set; } = GetActiveDate();
		readonly Clock _clock;
		DateTime _timestamp;
		Dictionary<PageViewModel, Event[]> _timetableToEvents;

		private static readonly double Density = DeviceDisplay.MainDisplayInfo.Density;
		private EventsLineRender.Options GetOptions(ResourceDictionary resources) => new()
		{
			Hours = new EventsLineRender.Text
			{
				Margin = new(16, 8, 4, 8),
				FontSize = resources.GetValue("HoursFontSize", 22.0001d) + Density / 1.5,
				FontScale = resources.GetValue("HoursFontScale", 1d),
				FontFamily = resources.GetValue("HoursFontFamily", ""),
				FontAttributes = resources.GetValue("HoursFontAttributes", FontAttributes.Bold)
			},

			Minutes = new EventsLineRender.Text
			{
				Margin = new(4),
				FontSize = resources.GetValue("MinutesFontSize", 15.0001d) + Density / 1.5,
				FontScale = resources.GetValue("MinutesFontScale", 1d),
				FontFamily = resources.GetValue("MinutesFontFamily", ""),
				FontAttributes = resources.GetValue("MinutesFontAttributes", FontAttributes.None)
			},
		};

		private object ProcessRow(ConvertArgs args)
		{
			if (_timetableToEvents.IsNot() ||
				args.Value.Is(out PageViewModel page).Not() ||
				_timetableToEvents.TryGetValue(page, out var rowHourEvents).Not())
				return default;

			var rowHour = Parse(page.Key) % 24;
			var resources = App.Current.Resources;
			return
				EventsLineRender.Construct(_timestamp, rowHour, rowHourEvents, _upcoming.Event, GetOptions(resources));
		}

		private object ProcessRows(ConvertArgs args)
		{
			var value = args.Value;

			if (value.Is(out IList<PageViewModel> rows).Not())
				return value;

			_timestamp = Clock.GetActiveDateTime();
			_timetableToEvents = rows.ToDictionary(row => row, row => GetEvents(row, ActiveDate));
			_upcoming = GetUpcomingInfo(_timetableToEvents, _timestamp);
			_openHour = default;

			async void ScrollToActiveItem()
			{
				await Task.Delay(100);
				TimetableListView.TryScrollTo(_upcoming.Item ?? _timetableToEvents.Keys.LastOrDefault());
			}

			if (_autoRefresh.Is(false))
				ScrollToActiveItem();

			_autoRefresh = false;

			return value;
		}

		private int? _openHour;
		private Event[] GetEvents(PageViewModel row, DateTime date)
		{
			var rowHour = Parse(row.Key) % 24;
			var offsetDays = _openHour.HasValue.Not() || _openHour < rowHour ? 0d : 1d;
			var rowHourEvents = row.Value
				.SplitByChars(" ").To(out var pieces)
				.Select(e => new Event
				{
					Key = e,
					Value = new DateTime
					(
						date.Year, date.Month, date.Day,
						rowHour, Parse(e) % 60, 30
					).AddDays(offsetDays)
				}).ToArray(pieces.Length);

			if (_openHour.HasValue.Not()) _openHour = rowHour;
			return rowHourEvents;
		}

		(PageViewModel Item, Event Event) _upcoming;
		private static (PageViewModel Item, Event Event) GetUpcomingInfo
			(Dictionary<PageViewModel, Event[]> timetableToEvents, DateTime timestamp) =>
			timetableToEvents.Select(p =>
			{
				var upcomingEvent = p.Value
					.Where(e => e.Value > timestamp)
					.OrderBy(e => e.Value)
					.FirstOrDefault();
				return (Row: p.Key, UpcomingEvent: upcomingEvent);
			}).FirstOrDefault(i => i.UpcomingEvent.Is());

		private readonly Regex regex = @"\d+".ToRegex();
		private int Parse(string s)
		{
			try { return s.TryParse(out int v) ? v : int.Parse(regex.Match(s).Value); }
			catch { return default; }
		}

		private void BookmarksButton_Clicked(object sender, EventArgs e) => BookmarksPicker.OnClick();

		private object Picker_GroupHeaderMaker() => new Rack
		{
			Rows = "^ ^",
			Children =
			{
				new Frame
				{
					HasShadow = false,
					CornerRadius = 5f,
					BackgroundColor = App.Get<Color>("BasicButtonColor"),
				}
				,
				new Label
				{
					Style = App.ItemLabelStyle,
					HorizontalOptions = LayoutOptions.Start,
					TextColor = App.Get<Color>("AccentTextColor"),
				}
				.Use(l => l.SetBinding(Label.TextProperty, new Binding { Path = "Key" }))
				.Use(l => l.SetBinding(Label.FontSizeProperty, new Binding { Path = "Text", Source=l, Converter=App.TextLengthToFontSizeConverter }))
				,
				new BoxView
				{
					Opacity = 0.8,
					Margin = new(10, 0),
					Style = App.Get<Style>("SeparatorStyle"),
					VerticalOptions = LayoutOptions.Start,
				}
			}
		};

		private object SourcePicker_ItemMaker() => new Rack
		{
			Rows = "^ ^",
			Children =
			{
				new Stack
				{
					Margin = new(5d, 5d, 5d, 0d),
					HorizontalOptions = LayoutOptions.Center,
					Orientation = StackOrientation.Horizontal,
				}
				.Use(s => Rack.SetCell(s, "R0"))
				.Use(s => BindableLayout.SetItemTemplate(s, (DataTemplate)App.Current.Resources["ParserKindTemplate"]))
				.Use(s => s.SetBinding(BindableLayout.ItemsSourceProperty, new Smart { Key = "Parsers" }))
				,
				new Label
				{
					Style = App.ItemLabelStyle,
					HorizontalOptions = LayoutOptions.End,
				}
				.Use(l => Rack.SetCell(l, "R1"))
				.Use(l => l.SetBinding(Label.TextProperty, new Binding { Path = "Key" }))
				.Use(l => l.SetBinding(Label.FontSizeProperty, new Binding { Path = "Text", Source=l, Converter=App.TextLengthToFontSizeConverter, ConverterParameter=18d }))
				,
				new Label
				{
					Style = App.ItemLabelStyle,
					HorizontalOptions = LayoutOptions.Start,
				}
				.Use(l => Rack.SetCell(l, "R1"))
				.Use(l => l.SetBinding(Label.TextProperty, new Smart { Key = "Status", Converter=App.SourceStatusToTextConverter, Mode=BindingMode.OneWay }))
				.Use(l => l.SetBinding(Label.TextColorProperty, new Smart { Key = "Status", Converter=App.SourceStatusToColorConverter, Mode=BindingMode.OneWay }))
				.Use(l => l.SetBinding(Label.FontSizeProperty, new Binding { Path = "Text", Source=l, Converter=App.TextLengthToFontSizeConverter, ConverterParameter=18d }))
				,
			}
		};

		private object RoutePicker_ItemMaker() => Page_ItemMaker(default, LayoutOptions.Center, false);
		private object DirectionPicker_ItemMaker() => Page_ItemMaker(App.TranslitConverter);
		private object StopPicker_ItemMaker() => Page_ItemMaker(App.TranslitConverter);

		private object Page_ItemMaker(IValueConverter textConverter, LayoutOptions horizontalOptions = default, bool flexFontSize = true) => new Rack
		{
			Children =
			{
				new Label { Style = App.ItemLabelStyle, HorizontalOptions = horizontalOptions }
				.Use(l => l.SetBinding(Label.TextProperty, new Binding { Path = "Value", Converter=textConverter }))
				.Use(l =>
				{
					if (flexFontSize)
						l.SetBinding(Label.FontSizeProperty, new Binding { Path = "Text", Source=l, Converter=App.TextLengthToFontSizeConverter });
				})
			}
		};
	}
}
