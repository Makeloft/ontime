﻿using Xamarin.Forms.Xaml;

namespace OnTime.Views.Settings
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class AdvertisingView
	{
		public AdvertisingView() => InitializeComponent();
	}
}