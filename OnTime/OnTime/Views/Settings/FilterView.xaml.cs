﻿using Xamarin.Forms.Xaml;

namespace OnTime.Views.Settings
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class FilterView
	{
		public FilterView() => InitializeComponent();
	}
}