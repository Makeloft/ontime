﻿using Xamarin.Forms.Xaml;

namespace OnTime.Views.Settings
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class VersionView
	{
		public VersionView() => InitializeComponent();
	}
}