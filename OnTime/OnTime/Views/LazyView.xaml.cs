﻿using Ace;

using System;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace OnTime.Views
{
	public partial class LazyView
	{
		public Type ContentViewType { get; set; }

		public LazyView()
		{
			InitializeComponent();

			SizeChanged += async (o, e) =>
			{
				if (ContentViewType.Is(Content?.GetType()))
					return;

				await Task.Delay(8);
				Content = (View)Activator.CreateInstance(ContentViewType);
			};
		}
	}
}
