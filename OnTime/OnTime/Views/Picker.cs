﻿using Ace;
using Ace.Controls;
using Ace.Extensions;
using Ace.Markup;
using Ace.Mathematics;

using System;
using System.ComponentModel;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace OnTime.Views
{
	public class Picker : Xamarin.Forms.Picker
	{
		public static readonly BindableProperty AllowActiveItemResetProperty
			= Type<Picker>.Create(v => v.AllowActiveItemReset);
		public bool AllowActiveItemReset
		{
			get => this.Get(false);
			set => this.Set(value);
		}

		public static readonly BindableProperty ItemTemplateProperty
			= Type<Picker>.Create(v => v.ItemTemplate);
		public DataTemplate ItemTemplate
		{
			get => this.Get(default(DataTemplate));
			set => this.Set(value);
		}

		public static readonly BindableProperty GroupHeaderTemplateProperty
			= Type<Picker>.Create(v => v.GroupHeaderTemplate);
		public DataTemplate GroupHeaderTemplate
		{
			get => this.Get(default(DataTemplate));
			set => this.Set(value);
		}

		public static readonly BindableProperty EmptyItemsTemplateProperty
			= Type<Picker>.Create(v => v.EmptyItemsTemplate);
		public ControlTemplate EmptyItemsTemplate
		{
			get => this.Get(default(ControlTemplate));
			set => this.Set(value);
		}

		public object SelectedValue => SelectedItem.Is(out var selectedItem)
			? GetDisplayValue(selectedItem, (Binding)ItemDisplayBinding)
			: default
			;

		public bool SkipAnimation { get; set; }

		public Picker()
		{
			var backgroundColor = BackgroundColor;
			var periodsCounter = 0;

			async void SetAnimationState()
			{
				await Task.Delay(128);

				if (SkipAnimation || SelectedItem.Is() || BindingContext.IsNot() || periodsCounter > 0)
					return;

				var app = Application.Current;
				var c = app.Is() ? (Color)app.Resources["Accent"] : default; //Color.Accent;

				periodsCounter = 128;

				Visualisation.Animate(
					from: -Math.PI,
					till: +Math.PI,
					framesCount: 128,
					frameDuration_Milliseconds: 8,

					action: value => BackgroundColor = new(c.R, c.G, c.B, value * 0.62),
					change: offset => (1d + Math.Cos(offset)) / 2d,
					repeat: () => 0 < periodsCounter-- && SelectedItem.IsNot() && SkipAnimation is false,
					finish: () =>
					{
						periodsCounter = 0;
						BackgroundColor = backgroundColor;
					});
			}

			BindingContextChanged += (o, e) => SetAnimationState();
			PropertyChanged += (o, e) =>
			{
				if (nameof(SelectedItem).Is(e.PropertyName))
					SetAnimationState();
			};
		}

		public static object GetDisplayValue(object item, Binding itemDisplayBinding)
		{
			if (itemDisplayBinding.IsNot())
				return item;

			var path = itemDisplayBinding.Path;
			var value = item is ViewModels.PageViewModel page && (nameof(page.Key).Is(path) || nameof(page.Value).Is(path))
				? nameof(page.Key).Is(path) ? page.Key : page.Value
				: item.GetPropertyValue(path).To<string>();

			return itemDisplayBinding.Converter.Is(out IValueConverter converter)
				? converter.Convert(value, default, default, default)
				: value;
		}

		public double ItemSize { get; set; } = 48d;
		public double ItemLength { get; set; } = 0d;
		public double GroupHeaderSize { get; set; } = 48d;
		public ScrollOrientation Orientation { get; set; } = ScrollOrientation.Vertical;

		public event ItemMakerDelegate ItemMaker;
		public event ItemMakerDelegate GroupHeaderMaker;

		public async void OnClick()
		{
			if (IsVisible.Is(false) || IsEnabled.Is(false))
				return;

			new PickerView { BindingContext = this }.To(out var pickerView);
			if (pickerView.GetSelectionSet().Is(out var setView))
			{
				setView.Orientation = Orientation;
				setView.AllowSelectedItemReset = AllowActiveItemReset;
				setView.GroupHeaderTemplate = GroupHeaderTemplate;
				setView.ItemTemplate = ItemTemplate;
				setView.GroupHeaderSize = GroupHeaderSize;
				setView.ItemLength = ItemLength;
				setView.ItemSize = ItemSize;
				setView.ItemMaker += ItemMaker;
				setView.GroupHeaderMaker += GroupHeaderMaker;
			}

			pickerView.GetEmptyItemsContentView().ControlTemplate = EmptyItemsTemplate;
			pickerView.GetSelectionSet().To(out var v).With
			(
				v.GroupHeaderTemplate = GroupHeaderTemplate,
				v.IsGroupingEnabled = GroupHeaderMaker.Is() || GroupHeaderTemplate.Is()
			);

			var backgroundColor = pickerView.BackgroundColor;
			var rootView = App.RootView;
			var overlay = new Rack
			{
				Rows = "* 50",
				Children =
				{
					pickerView.Use(v => Rack.SetCell(v, "R0")),
					new BannerView
					{
						BackgroundColor = backgroundColor
					}.Use(v => Rack.SetCell(v, "R1")),
					new AdMobView
					{
						IsVisible = Ace.Store.Get<AppViewModel>().AllowAdvertising,
						BackgroundColor = backgroundColor,
						AdUnitId = App.AdMobPickerUnit
					}.Use(v => Rack.SetCell(v, "R1"))
				}
			};

			rootView.ShowOverlay(overlay);

			int counter = 0;
			async void ItemsSourceChanged(object sender, PropertyChangedEventArgs args)
			{
				if (nameof(ItemsSource).IsNot(args.PropertyName))
					return;

				counter++;
				await Task.Delay(16);
				if (--counter > 0) return;

				pickerView.RefreshSelectionSet(tryKeepScrollPosition: true);
			};

			PropertyChanged += ItemsSourceChanged;

			var selectedItem = await pickerView.SelectedItemSource.Task;
			if (selectedItem.Is()) SelectedItem = selectedItem;

			overlay.IsEnabled = false;

			await Task.Delay(96);
			rootView.HideOverlay(overlay);

			overlay.IsEnabled = true;

			PropertyChanged += ItemsSourceChanged;

			if (selectedItem.Is())
			{
				Yandex.Metrica.YandexMetrica.Report($"✔️", selectedItem.To<string>());
			}
			else
			{
				Yandex.Metrica.YandexMetrica.Report($"✖", BindingContext.To<string>());
			}
		}
	}
}
