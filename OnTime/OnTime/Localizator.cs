﻿using Ace;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;

namespace OnTime
{
	public struct o { public string En, Ru, Be, Uk; };

	public enum LanguageCodes { English, Russian, Belorussian, Ukrainian };

	public static class Localizator
	{
		private static o _
		(
			string en = default,
			string ru = default,
			string be = default,
			string uk = default
		) => new()
		{
			En = en,
			Ru = ru ?? en,
			Be = be ?? ru ?? en,
			Uk = uk ?? ru ?? en,
		};

		public static o OnTime = _("On Time", "Вовремя", "Падчас", "Підчас");

		public static o Region = _("Region", "Регион", "Рэгіён", "Регіон");
		public static o Source = _("Source", "Источник", "Крыніца", "Джерело");
		public static o Kind = _("Kind", "Вид", "Від", "Вид");
		public static o Route = _("Route", "Маршрут", "Маршрут", "Маршрут");
		public static o Direction = _("Direction", "Направление", "Напрамак", "Напрям");
		public static o Stop = _("Stop", "Остановка", "Прыпынак", "Зупинка");
		public static o Day = _("Day", "День", "Дзень", "День");

		public static o Bookmark = _("Bookmark", "Закладка", "Закладка", "Закладка");
		public static o Remark = _("Remark", "Ремарка", "Рэмарка", "Ремарка");
		public static o Empty = _("Empty", "Пусто", "Пуста", "Пусто");

		public static o Question = _("Question", "Вопрос", "Пытанне", "Питання");
		public static o Error = _("Error", "Ошибка", "Памылка", "Помилка");
		public static o Close = _("Close", "Закрыть", "Закрыць", "Закрити");
		public static o Cancel = _("Cancel", "Отменить", "Адмяніць", "Скасувати");
		public static o Accept = _("Accept", "Принять", "Прыняць", "Прийняти");
		public static o Discard = _("Discard", "Сбросить", "Зкінуць", "Скинути");
		public static o Repeat = _("Repeat", "Повторить", "Паўтарыць", "Повторити");
		public static o Skip = _("Skip", "Пропустить", "Прапусціць", "Пропустити");
		public static o Yes = _("Yes", "Да", "Так", "Так");
		public static o No = _("No", "Нет", "Не", "Ні");

		public static o Bus = _("Bus", "Автобус", "Аўтобус", "Автобус");
		public static o Trolleybus = _("Trolleybus", "Троллейбус", "Тралейбус", "Тролейбус");
		public static o Tram = _("Tram", "Трамвай", "Трамвай", "Трамвай");
		public static o Metro = _("Metro", "Метро", "Метро", "Метро");
		public static o Minibus = _("Minibus", "Маршрутка", "Маршрутка");

		public static o Belarus = _("Belarus", "Беларусь", "Беларусь", "Білорусь");
		public static o Ukrain = _("Ukrain", "Украина", "Украіна", "Україна");
		public static o Russia = _("Russia", "Россия", "Расія", "Росія");
		public static o Crimea = _("Crimea", "Крым", "Крым", "Крим");

		public static o MoscowRegion = _("Moscow Region", "Московский регион", "Маскоўскі рэгіён", "Московський регіон");
		public static o VolgogradRegion = _("Volgograd Region", "Волгоградский регион", "Валгаградскі рэгіён", "Волгоградський регіон");
		public static o KemerovoRegion = _("Kemerovo Region", "Кемеровский регион", "Кемераўскі рэгіён", "Кемеровський регіон");
		public static o KrasnoyarskRegion = _("Krasnoyarsk Region", "Красноярский регион", "Краснаярскі рэгіён", "Красноярський регіон");
		public static o TyumenRegion = _("Tyumen Region", "Тюменский регион", "Цюменьскі рэгіён", "Тюменський регіон");

		public static o Moscow = _("Moscow", "Москва", "Масква");
		public static o Vologda = _("Vologda", "Вологда", "Волагда");
		public static o Yekaterinburg = _("Yekaterinburg", "Екатеринбург", "Екацерынбург");
		public static o Kazan = _("Kazan", "Казань", "Казань");
		public static o Kaliningrad = _("Kaliningrad", "Калининград", "Калінінград");
		public static o Kirov = _("Kirov", "Киров", "Кіраў");
		public static o Krasnodar = _("Krasnodar", "Краснодар", "Краснадар");
		public static o Magnitogorsk = _("Magnitogorsk", "Магнитогорск", "Магнітагорск");
		public static o Murmansk = _("Murmansk", "Мурманск", "Мурманск");
		public static o NizhniyNovgorod = _("Nizhniy Novgorod", "Нижний Новгород", "Ніжні Ноўгарад");
		public static o Novorossiysk = _("Novorossiysk", "Новороссийск", "Новарасійск");
		public static o Novosibirsk = _("Novosibirsk", "Новосибирск", "Новасібірск");
		public static o Omsk = _("Omsk", "Омск", "Омск");
		public static o Perm = _("Perm", "Пермь", "Пермь");
		public static o Pskov = _("Pskov", "Псков", "Пскоў");
		public static o RostovonDon = _("Rostov-on-Don", "Ростов-на-Дону", "Растоў-на-Дану");
		public static o Smolensk = _("Smolensk", "Смоленск", "Смаленск");
		public static o Khabarovsk = _("Khabarovsk", "Хабаровск", "Хабараўск");
		public static o Chelyabinsk = _("Chelyabinsk", "Челябинск", "Чэлябінск");
		public static o Yaroslavl = _("Yaroslavl", "Ярославль", "Яраслаўль");

		public static o Balashikha = _("Balashikha", "Балашиха");
		public static o Barybino = _("Barybino", "Барыбино");
		public static o Vereya = _("Vereya", "Верея");
		public static o Vidnoye = _("Vidnoye", "Видное");
		public static o Volokolamsk = _("Volokolamsk", "Волоколамск");
		public static o Voskresensk = _("Voskresensk", "Воскресенск");
		public static o Dmitrov = _("Dmitrov", "Дмитров");
		public static o Dolgoprudny = _("Dolgoprudny", "Долгопрудный");
		public static o Domodedovo = _("Domodedovo", "Домодедово");
		public static o Yegoryevsk = _("Yegoryevsk", "Егорьевск");
		public static o Zhukovsky = _("Zhukovsky", "Жуковский");
		public static o Zvenigorod = _("Zvenigorod", "Звенигород");
		public static o Ivanteyevka = _("Ivanteyevka", "Ивантеевка");
		public static o Istra = _("Istra", "Истра");
		public static o Kashira = _("Kashira", "Кашира");
		public static o Klin = _("Klin", "Клин");
		public static o Kolomna = _("Kolomna", "Коломна");
		public static o Korolyov = _("Korolyov", "Королев");
		public static o Kurovskoe = _("Kurovskoe", "Куровское");
		public static o LosinoPetrovsky = _("Losino-Petrovsky", "Лосино-Петровский", "Losino-Petrovsky");
		public static o Lukhovitsy = _("Lukhovitsy", "Луховицы");
		public static o Lytkarino = _("Lytkarino", "Лыткарино");
		public static o Lyubertsy = _("Lyubertsy", "Люберцы");
		public static o Malino = _("Malino", "Малино");
		public static o Mozhaysk = _("Mozhaysk", "Можайск");
		public static o Mytischi = _("Mytischi", "Мытищи");
		public static o NaroFominsk = _("Naro-Fominsk", "Наро-Фоминск", "Naro-Fominsk");
		public static o Noginsk = _("Noginsk", "Ногинск");
		public static o Odintsovo = _("Odintsovo", "Одинцово");
		public static o Ozyory = _("Ozyory", "Озёры");
		public static o OrekhovoZuyevo = _("Orekhovo-Zuyevo", "Орехово-Зуево", "Orekhovo-Zuyevo");
		public static o PavlovskiyPosad = _("Pavlovskiy Posad", "Павловский Посад", "Pavlovskiy Posad");
		public static o Podolsk = _("Podolsk", "Подольск");
		public static o Ramenskoye = _("Ramenskoye", "Раменское");
		public static o Roshal = _("Roshal", "Рошаль");
		public static o Ruza = _("Ruza", "Руза");
		public static o SergiyevPosad = _("Sergiyev Posad", "Сергиев Посад", "Sergiyev Posad");
		public static o SerebryanyyePrudy = _("Serebryanyye Prudy", "Серебряные пруды", "Serebryanyye Prudy");
		public static o Serpukhov = _("Serpukhov", "Серпухов");
		public static o Solnechnogorsk = _("Solnechnogorsk", "Солнечногорск");
		public static o Stupino = _("Stupino", "Ступино");
		public static o Taldom = _("Taldom", "Талдом");
		public static o Fryanovo = _("Fryanovo", "Фряново");
		public static o Khimki = _("Khimki", "Химки");
		public static o Chekhov = _("Chekhov", "Чехов");
		public static o Shatura = _("Shatura", "Шатура");
		public static o Shakhovskaya = _("Shakhovskaya", "Шаховская");
		public static o Schyolkovo = _("Schyolkovo", "Щёлково");
		public static o Elektrostal = _("Elektrostal", "Электросталь");

		public static o Volgograd = _("Volgograd", "Волгоград");
		public static o Volzhsky = _("Volzhsky", "Волжский");
		public static o Dubovka = _("Dubovka", "Дубовка");
		public static o Kamyshin = _("Kamyshin", "Камышин");
		public static o Krasnoslobodsk = _("Krasnoslobodsk", "Краснослободск");
		public static o Novoanninsky = _("Novoanninsky", "Новоаннинский");
		public static o Uryupinsk = _("Uryupinsk", "Урюпинск");
		public static o Frolovo = _("Frolovo", "Фролово");
		public static o Frolovskiy = _("Frolovskiy", "Фроловский");

		public static o Kemerovo = _("Kemerovo", "Кемерово");
		public static o AnzheroSudzhensk = _("Anzhero-Sudzhensk", "Анжеро-Судженск", "Anzhero-Sudzhensk");
		public static o Belovo = _("Belovo", "Белово");
		public static o Beryozovskiy = _("Beryozovskiy", "Березовский");
		public static o Izhmorskiy = _("Izhmorskiy", "Ижморский");
		public static o Kiselyovsk = _("Kiselyovsk", "Киселёвск");
		public static o Krapivinskiy = _("Krapivinskiy", "Крапивинский");
		public static o LeninskKuznetskiy = _("Leninsk-Kuznetskiy", "Ленинск-Кузнецкий", "Leninsk-Kuznetskiy");
		public static o Mariinsk = _("Mariinsk", "Мариинск");
		public static o Mezhdurechensk = _("Mezhdurechensk", "Междуреченск");
		public static o Myski = _("Myski", "Мыски");
		public static o Osinniki = _("Osinniki", "Осинники");
		public static o Prokopyevsk = _("Prokopyevsk", "Прокопьевск");
		public static o Promyshlennovskiy = _("Promyshlennovskiy", "Промышленновский");
		public static o Tayga = _("Tayga", "Тайга");
		public static o Tashtagol = _("Tashtagol", "Таштагол");
		public static o Tisulskiy = _("Tisulskiy", "Тисульский");
		public static o Topki = _("Topki", "Топки");
		public static o Tyazhinskiy = _("Tyazhinskiy", "Тяжинский");
		public static o Yurga = _("Yurga", "Юрга");
		public static o Yashkinskiy = _("Yashkinskiy", "Яшкинский");

		public static o Tobolsk = _("Tobolsk", "Тобольск");
		public static o Zavodoukovsk = _("Zavodoukovsk", "Заводоуковск");
		public static o Ishim = _("Ishim", "Ишим");
		public static o Yalutorovsk = _("Yalutorovsk", "Ялуторовск");

		public static o Achinsk = _("Achinsk", "Ачинск");
		public static o Zheleznogorsk = _("Zheleznogorsk", "Железногорск");
		public static o Uyar = _("Uyar", "Уяр");

		public static o Saki = _("Saki", "Саки");
		public static o Sevastopol = _("Sevastopol", "Севастополь");
		public static o Simferopol = _("Simferopol", "Симферополь");
		public static o Kerch = _("Kerch", "Керчь");

		public static o Kyiv = _("Kyiv", "Киев", "Кіеў", "Київ");

		public static o Minsk = _("Minsk", "Минск", "Мінск");
		public static o Gomel = _("Gomel", "Гомель", "Гомель");
		public static o Mogilev = _("Mogilev", "Могилёв", "Магілёў");
		public static o Vitebsk = _("Vitebsk", "Витебск", "Віцебск");
		public static o Grodno = _("Grodno", "Гродно", "Гродна");
		public static o Brest = _("Brest", "Брест", "Брэст");
		public static o Bobruisk = _("Bobruisk", "Бобруйск", "Бабруйск");
		public static o Baranovichi = _("Baranovichi", "Барановичи", "Баранавічы");
		public static o Borisov = _("Borisov", "Борисов", "Барысаў");
		public static o Pinsk = _("Pinsk", "Пинск", "Пінск");
		public static o Orsha = _("Orsha", "Орша", "Орша");
		public static o Lida = _("Lida", "Лида", "Ліда");
		public static o Soligorsk = _("Soligorsk", "Солигорск", "Салігорск");
		public static o Molodechno = _("Molodechno", "Молодечно", "Маладзечна");
		public static o Kobrin = _("Kobrin", "Кобрин", "Кобрын");
		public static o Volkovysk = _("Volkovysk", "Волковыск", "Ваўкавыск");
		public static o Vilejka = _("Vilejka", "Вилейка", "Вілейка");
		public static o Glubokoe = _("Glubokoe", "Глубокое", "Глыбокае");
		public static o Smorgon = _("Smorgon", "Смогонь", "Смаргонь");
		public static o Oshmjany = _("Oshmjany", "Ошмяны", "Ашмяны");
		public static o Mjadel = _("Mjadel", "Мядель", "Мядзель");
		public static o Volozhin = _("Volozhin", "Воложин", "Валожын");
		public static o Postavy = _("Postavy", "Поставы", "Паставы");

		public static o Sunday = _("Sunday", "Воскресенье", "Нядзеля", "Неділя");
		public static o Monday = _("Monday", "Понедельник", "Панядзелак", "Понеділок");
		public static o Tuesday = _("Tuesday", "Вторник", "Аўторак", "Вівторок");
		public static o Wednesday = _("Wednesday", "Среда", "Серада", "Середа");
		public static o Thursday = _("Thursday", "Четверг", "Чацвер", "Четвер");
		public static o Friday = _("Friday", "Пятница", "П'ятниця");
		public static o Saturday = _("Saturday", "Суббота", "Субота", "Субота");

		public static o Workdays = _("Workdays", "Будни", "Будні", "Будні");
		public static o Weekends = _("Weekends", "Выходные", "Выхадныя", "Вихідні");
		public static o Everyday = _("Everyday", "Ежедневно", "Штодзённа", "Щодня");

		public static o Settings = _("Settings", "Настройки", "Наладкі", "Налаштування");
		public static o Theme = _("Theme", "Тема", "Тэма", "Тема");
		public static o Palette = _("Palette", "Палитра", "Палітра", "Палітра");
		public static o Font = _("Font", "Шрифт", "Шрыфт", "Шрифт");
		public static o Basic = _("Basic", "Базовый", "Базавы", "Базовий");
		public static o Timetable = _("Timetable", "Расписание", "Расклад", "Розклад");
		public static o Minutes = _("Minutes", "Минуты", "Хвіліны", "Хвилини");
		public static o Hours = _("Hours", "Часы", "Гадзіны", "Години");
		public static o Tab = _("Tab", "Вкладка", "Ўкладка", "Вкладка");
		public static o ActiveTab = _("Active Tab", "Активная вкладка", "Актыўная ўкладка", "Активна вкладка");
		public static o Name = _("Name", "Имя", "Імя", "Ім'я");
		public static o Scale = _("Scale", "Масштаб", "Маштаб", "Масштаб");
		public static o Family = _("Family", "Семейство", "Сямейства", "Сімейство");
		public static o Attributes = _("Attributes", "Атрибуты", "Атрыбуты", "Атрибути");
		public static o Bold = _("Bold", "Жирный", "Тлусты", "Жирни");
		public static o Italic = _("Italic", "Курсив", "Курсіў", "Курсив");
		public static o BoldItalic = _("Bold Italic", "Жирный курсив", "Тлусты курсіў", "Жирний курсив");
		public static o Localization = _("Localization", "Локализация", "Лакалізацыя", "Локалізація");
		public static o Language = _("Language", "Язык", "Мова", "Мова");
		public static o Advertising = _("Advertising", "Реклама", "Рэклама", "Реклама");
		public static o Transliteration = _("Transliteration", "Транслитерация", "Транслітарацыя", "Транслітерація");
		public static o Intensity = _("Intensity", "Интенсивность", "Інтэнсіўнасць", "Інтенсивність");
		public static o Donate = _("Donate", "Пожертвовать", "Ахвяраваць", "Пожертвувати");
		public static o Filter = _("Filter", "Фильтр", "Фільтр", "Фільтр");
		public static o Color = _("Color", "Цвет", "Колер", "Колір");
		public static o Version = _("Version", "Версия", "Версія", "Версія");
		public static o Number = _("Number", "Номер", "Нумар", "Номер");
		public static o Value = _("Value", "Значение", "Значэнне");
		public static o More = _("More", "Ещё", "Яшчэ", "Ще");
		public static o Rate = _("Rate", "Оценить", "Ацаніць", "Оцінити");
		public static o Done = _("Done", "Сделано", "Зроблена", "Зроблено");
		public static o None = _("None", "Нету", "Няма", "Немає");
		public static o Key = _("Key", "Ключ", "Ключ", "Ключ");

		public static o English = _("English", "Английский", "Англійская", "Англійська");
		public static o Russian = _("Russian", "Русский", "Руская", "Руська");
		public static o Ukrainian = _("Ukrainian", "Украинский", "Украінская", "Українська");
		public static o Belorussian = _("Belorussian", "Белорусский", "Беларуская", "Білоруська");

		public static o NoTraffic = _("No Traffic", "Нет движения", "Няма руху", "Немає руху");

		public static o CheckInternetConnectionMessage = _
		(
			Messages.CheckInternetConnectionMessage.En,
			Messages.CheckInternetConnectionMessage.Ru,
			Messages.CheckInternetConnectionMessage.Be,
			Messages.CheckInternetConnectionMessage.Uk
		);

		public static o RemoveQuestion = _
		(
			Messages.RemoveQuestion.En,
			Messages.RemoveQuestion.Ru,
			Messages.RemoveQuestion.Be,
			Messages.RemoveQuestion.Uk
		);

		public static o RateQuestion = _
		(
			Messages.RateQuestion.En,
			Messages.RateQuestion.Ru,
			Messages.RateQuestion.Be,
			Messages.RateQuestion.Uk
		);

		public static o OpenBonusColorPalettesMessage = _
		(
			Messages.OpenBonusColorPalettesMessage.En,
			Messages.OpenBonusColorPalettesMessage.Ru,
			Messages.OpenBonusColorPalettesMessage.Be,
			Messages.OpenBonusColorPalettesMessage.Uk
		);

		public static o AllowAdvertisingMessage = _
		(
			Messages.AllowAdvertisingMessage.En,
			Messages.AllowAdvertisingMessage.Ru,
			Messages.AllowAdvertisingMessage.Be,
			Messages.AllowAdvertisingMessage.Uk
		);

		public static Dictionary<string, o> GetBaseDictionary() =>
			typeof(Localizator).GetFields(BindingFlags.Static | BindingFlags.Public).
			ToDictionary(m => m.Name, m => (o)typeof(Localizator).GetField(m.Name).GetValue(null));

		public static Dictionary<string, string> GetDictionary(LanguageCodes code) =>
			GetBaseDictionary().ToDictionary(p => p.Key, p => code switch
			{
				LanguageCodes.English => p.Value.En,
				LanguageCodes.Russian => p.Value.Ru,
				LanguageCodes.Ukrainian => p.Value.Uk,
				LanguageCodes.Belorussian => p.Value.Be,
				_ => throw new NotImplementedException(),
			});
	}

	public class LanguageManager : ResourceManager
	{
		public LanguageManager(LanguageCodes code) => _keyToValue = Localizator.GetDictionary(code);

		private readonly Dictionary<string, string> _keyToValue;
		public override string GetString(string key) => _keyToValue.GetValue(key);
		public override string GetString(string key, CultureInfo culture) => _keyToValue.GetValue(key);
	}

	public static class Messages
	{
		public static class CheckInternetConnectionMessage
		{
			public const string En = @"Check Internet Connection";
			public const string Ru = @"Проверьте интернет-подключение";
			public const string Be = @"Праверце інтэрнэт-падключэнне";
			public const string Uk = @"Перевірте інтэрнэт-підключення";
		}

		public static class RemoveQuestion
		{
			public const string En = "Remove the item?";
			public const string Ru = "Удалить этот элемент?";
			public const string Be = "Выдаліць гэты элемент?";
			public const string Uk = "Видалити цей елемент?";
		}

		public static class RateQuestion
		{
			public const string En = "Are you ready to leave a review about the application?\n\nEach review affects to the count of downloads and gives motivation to improve it!";
			public const string Ru = "Готовы ли вы оставить отзыв о приложении?\n\nКаждый отзыв влияет на число загрузок и придаёт мотивацию его улучшать!";
			public const string Be = "Ці гатовыя вы пакінуць водгук аб дадатку?\n\nКожны водгук ўплывае на лік загрузак ды надае матывацыю яго паляпшаць!";
			public const string Uk = "Чи готові ви залишити відгук про програму?\n\nКожен відгук впливає на кількість завантажень і надає мотивацію його покращувати!";
		}

		public static class OpenBonusColorPalettesMessage
		{
			public const string En = "Please, rate or review the app to open a bonus set of color palettes";
			public const string Ru = "Пожалуйста, оцените приложение или оставьте отзыв, чтобы открыть бонусный набор цветовых палитр.";
			public const string Be = "Калі ласка, ацаніце дадатак або пакіньце водгук, каб адкрыць бонусны набор каляровых палітраў.";
			public const string Uk = "Будь ласка, оцініть програму або залиште відгук, щоб відкрити бонусний набір колірних палітр.";
		}
		
		public static class AllowAdvertisingMessage
		{
			public const string En = "Please, rate the app or make a voluntary donation to hide advertising and support developing";
			public const string Ru = "Пожалуйста, оцените приложение или совершите добровольное пожертвование, чтобы скрыть рекламу и поддержать разработку.";
			public const string Be = "Калі ласка, ацаніце дадатак або здзейсніце добраахвотнае ахвяраванне, каб схаваць рэкламу і падтрымаць распрацоўку.";
			public const string Uk = "Будь ласка, оцініть програму або здійсніть добровільну пожертву, щоб приховати рекламу та підтримати розробку.";
		}
	}
}
