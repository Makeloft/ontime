﻿using Ace;
using System.Threading.Tasks;
#if WPF
using System.Windows;
using FontAttributes = System.Windows.FontWeight;
#else
using Xamarin.Essentials;
using Xamarin.Forms;
#endif

namespace OnTime
{
	[DataContract]
	public class FontInfo : ContextObject, IExposable
	{
#if WPF
		private static readonly double Density = 1.0;
#else
		private static readonly double Density = DeviceDisplay.MainDisplayInfo.Density;
#endif

		public FontInfo() => Expose();
		public FontInfo(string key) : this() => Key = key;
		public FontInfo(string key, double size) : this(key) => Size = size;
		public FontInfo(string key, double size, string family) : this(key, size) => Family = family;
		public FontInfo(string key, double size, string family, FontAttributes attributes) : this(key, size, family) =>
			Attributes = attributes;

		[DataMember]
		public string Key { get; set; }
		[DataMember]
		public double Size { get; set; }

		[DataMember]
		public string Family
		{
			get => Get(() => Family);
			set => Set(() => Family, value ?? Family, true);
		}

		[DataMember]
		public double Scale
		{
			get => Get(() => Scale, 1d);
			set => Set(() => Scale, value, true);
		}

		[DataMember]
		public FontAttributes Attributes
		{
			get => Get(() => Attributes, default);
			set => Set(() => Attributes, value, true);
		}

		public void Expose()
		{
			var counter = 0;
			PropertyChanged += async (o, e) =>
			{
				++counter;
				await Task.Delay(512);
				if (--counter > 0) return;

				Apply();
				Refresh();
			};
		}

		public void Apply() => Apply(App.Current.Resources);

		public void Apply(ResourceDictionary resources)
		{
			resources[$"{Key}FontSize"] = (Size + 0.0001 + Density / 1.5d) * Scale;
			resources[$"{Key}FontScale"] = Scale;
			resources[$"{Key}FontFamily"] = Family;
			resources[$"{Key}FontAttributes"] = Attributes;
		}

		public static void Refresh()
		{
			var appViewModel = Store.Get<AppViewModel>();
			appViewModel.FontFamilies = AppViewModel.LoadFontFamilies();
			appViewModel.ResetLocalization();
			AppViewModel.ForceUIRefresh_Hack();
		}
	}
}
