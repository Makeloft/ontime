﻿using Ace;
using Ace.Controls;

using System.Collections;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using OnTime.ViewModels;
using OnTime.Views;

namespace OnTime.Palettes
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class Templates
	{
		public Templates() => InitializeComponent();

		private void SourceGroupHeader_Tapped(object sender, System.EventArgs e) =>
			App.RootView.ActiveOverlay.To<Rack>()?.Children.OfType<PickerView>().FirstOrDefault().To(out var pickerView)?.With
			(
				pickerView.TrySetResult(sender.To<View>().BindingContext.To<IEnumerable>().Cast<PageViewModel>().FirstOrDefault())
			);
	}
}
