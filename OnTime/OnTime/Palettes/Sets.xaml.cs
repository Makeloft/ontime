﻿using Xamarin.Forms.Xaml;

namespace OnTime.Palettes
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class Sets
	{
		public Sets() => InitializeComponent();
	}
}
