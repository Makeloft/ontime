﻿using Ace;
using System.Collections.Generic;
using Xamarin.Forms;
using static Xamarin.Forms.Color;

namespace OnTime.Palettes
{
	public class Colors : ResourceDictionary
	{
		public static Dictionary<string, (string, string, string, string)> KeyToColors = New.Dictionary
		(
			("#ECF5ED", "#AEC5AA", "#5A9D2D", "#003100").By("Mint"),
			("#FFFAF6", "#FFD1BB", "#E4815D", "#9C4B28").By("Powder"),
			("#FBFCFD", "#DEEFED", "#93A7A6", "#5F6564").By("Snow"),
			("#FFFFFF", "#D3D3D3", "#A9A9A9", "#696969").By("Cloud"),
			("#E0E0E2", "#333333", "#AAAAAE", "#495464").By("Fog"),
			("#FFD1BB", "#FDB195", "#E4815D", "#9C4B28").By("Peach"),
			("#C4DFE6", "#07575B", "#66A5AD", "#003B46").By("Wave"),
			("#563E20", "#B38540", "#7E7B15", "#EBDF00").By("Pear"),
			("#C7D396", "#61485A", "#7F8E77", "#D1537F").By("Straw"),
			("#003B46", "#07575B", "#66A5AD", "#C4DFE6").By("Ocean"),
			("#0F1F38", "#1B4B5A", "#8E7970", "#F55449").By("Midnight"),
			("#090705", "#340F00", "#6B2304", "#FF3F07").By("Candle"),
			("#262F34", "#615049", "#F34A4A", "#F1D3BC").By("Soil"),
			("#121212", "#333333", "#696969", "#D3D3D3").By("Sood"),

			("#FCFDFE", "#A5C05B", "#F69454", "#EE693F").By("Carrot"),
			("#FCFDFE", "#F5CA99", "#FE7A47", "#D8412F").By("Strawberry"),
			("#FEF3E2", "#FAAF08", "#FA812F", "#FA4032").By("Orange"),
			("#EBDCB2", "#DDBC95", "#B38867", "#626D71").By("Latte"),
			("#301B28", "#523634", "#B6452C", "#DDC5A2").By("Chocolate"),
			("#1D3252", "#2D4262", "#C2C2C2", "#FFFFFF").By("Peak"),
			("#232122", "#131112", "#7BA4A8", "#A5C05B").By("Neon")

			//("#F9F9FF", "#FFA577", "#D55448", "#896E69").By("Melon"),
			//("#E2DFA2", "#A1BE95", "#92AAC7", "#ED5752").By("Nature"),
			//("#FFD64D", "#9BC01C", "#FA6775", "#F52549").By("Tropic"),
			//("#DAC3B3", "#CDAB81", "#6C5F5B", "#4F4A45").By("Paper-1"),
			//("#6C5F5B", "#4F4A45", "#CDAB81", "#DAC3B3").By("Paper-2"),
			//("#EC96A4", "#DFE166", "#9A9EAB", "#5D535E").By("Birds"),
			//("#BAA896", "#E6CCB5", "#E38B75", "#FCC875").By("P-1"),
			//("#011A27", "#063852", "#F0810F", "#E6DF44").By("P-2"),
		);

		public static Colors Create(string key) => KeyToColors.TryGetValue(key, out var value)
			? new(value.Item1, value.Item2, value.Item3, value.Item4)
			: new();

		public Colors() : this("#003B46") { }
		public Colors
		(
			string appBackground = "#003B46",
			string basicButton = "#07575B",
			string basicText = "#66A5AD",
			string accentText = "#C4DFE6",
			string selection = "#FF8C00",
			string workday = default,
			string weekend = default,
			string glass = "#DD333333",
			string accent = "#FF4081"
		)
		{
			this["AppBackgroundColor"] = FromHex(appBackground);
			this["BasicButtonColor"] = FromHex(basicButton);
			this["BasicTextColor"] = FromHex(basicText);
			this["AccentTextColor"] = FromHex(accentText);
			this["SelectionColor"] = FromHex(selection);
			this["WorkdayColor"] = FromHex(workday ?? accentText);
			this["WeekendColor"] = FromHex(weekend ?? accent);
			this["GlassColor"] = FromHex(glass);
			this["Accent"] = FromHex(accent);
		}
	}
}
