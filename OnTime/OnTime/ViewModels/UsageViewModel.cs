﻿using Ace;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;

namespace OnTime.ViewModels
{
	[DataContract]
	public class Mark
	{
		[DataMember] public string Key { get; set; }
		[DataMember] public string GroupKey { get; set; }
		[DataMember] public string Remark { get; set; }

		[DataMember] public string Source { get; set; }
		[DataMember] public string Kind { get; set; }
		[DataMember] public string Route { get; set; }
		[DataMember] public string Direction { get; set; }
		[DataMember] public string Stop { get; set; }

		public override string ToString() => $"{Route} {Direction} {Stop}";
	}

	public class UsageViewModel : ContextObject, IExposable
	{
		public async void Expose()
		{
			PageViewModel.PageActivated += page =>
			{
				var marks = Marks;
				var isStop = (page?.Root?.Root?.Root?.Root?.Root).Is(out var root) && root.Root.IsNot();
				if (isStop.Not() || marks.IsNot())
					return;

				if (marks.FirstOrDefault(i => i.Key.Is(page.FullKey)).Is(out var activeMark))
				{
					//ActiveMark = activeMark;
					return;
				}

				var mark = new Mark
				{
					Source = root.ActiveItem.To(out var source).Key,
					Kind = source.ActiveItem.To(out var kind).Value,
					Route = kind.ActiveItem.To(out var route).Value,
					Direction = route.ActiveItem.To(out var direction).Value,
					Stop = direction.ActiveItem.To(out var stop).Value,

					Key = page.FullKey,
					GroupKey = page.Root.FullKey,
				};

				marks.Insert(0, mark);
				//ActiveMark = mark;
			};

			this[Context.Get("Down")].Executed += args => Move(Marks, args.Parameter, +1);
			this[Context.Get("Up")].Executed += args => Move(Marks, args.Parameter, -1);

			this[Context.Get("Remove")].Executed += async args =>
			{
				if (App.RootView.Is(out var view) && Marks.Contains(args.Parameter))
				{
					var accept = "Yes".Localize();
					var cancel = "No".Localize();
					var title = "Question".Localize();
					var message = "RemoveQuestion".Localize();
					var result = await view.DisplayAlert(title, message, accept, cancel);
					if (result.IsTrue())
					{
						Marks.Remove(args.Parameter);
					}
				}
			};

			var timetableViewModel = Store.Get<TimetableViewModel>();
			this[() => ActiveMark].Changed += async args =>
			{
				if (ActiveMark.IsNot())
					return;

				await Task.Delay(64);

				var keys = ActiveMark.Key.SplitByChars("•");
				var item = timetableViewModel.Root;
				for (var i = 1; i < 6 && item.Is(); i++)
					(await item?.Activate(keys[i])).To(out item);

				await Task.Delay(50);
				ActiveMark = default;
			};

			Marks = await Task.Factory.StartNew(() => Store.Get<SmartSet<Mark>>()) ?? new SmartSet<Mark>();

			void SyncActiveRegionMarks()
			{
				var sources = timetableViewModel.Root?.Items;
				ActiveRegionMarks = sources.Is()
					? Marks.Where(m => sources.Any(i => i.Key.Is(m.Source))).ToSet()
					: default
					;
			}

			SyncActiveRegionMarks();

			var appViewModel = Store.Get<AppViewModel>();
			appViewModel[() => appViewModel.ActiveRegion].Changed += args => SyncActiveRegionMarks();
			Marks.CollectionChanged += (o, e) => SyncActiveRegionMarks();
		}

		private static void Move(IList items, object item, int step)
		{
			var index = items.IndexOf(item);
			var target = (index + step) % items.Count;
			items.RemoveAt(index);
			items.Insert(target, item);		
		}

		public Mark ActiveMark
		{
			get => Get(() => ActiveMark);
			set => Set(() => ActiveMark, value, true);
		}

		public SmartSet<Mark> Marks { get; set; }

		public SmartSet<Mark> ActiveRegionMarks
		{
			get => Get(() => ActiveRegionMarks);
			set => Set(() => ActiveRegionMarks, value, true);
		}
	}
}
