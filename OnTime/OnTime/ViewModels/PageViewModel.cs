﻿using Ace;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Yandex.Metrica;

namespace OnTime.ViewModels
{
	[DataContract]
	public class PageViewModel : ContextObject, IExposable
	{
		private readonly Func<PageViewModel, Task<IEnumerable<PageViewModel>>> _itemsProvider;

		private readonly Func<IEnumerable<PageViewModel>, PageViewModel> _activeItemSelector;

		public PageViewModel() { }

		public PageViewModel(PageViewModel root, string key, string value,
			Func<PageViewModel, Task<IEnumerable<PageViewModel>>> itemsProvider,
			Func<IEnumerable<PageViewModel>, PageViewModel> activeItemSelector = default)
		{
			Root = root;
			Key = key;
			Value = value;
			_itemsProvider = itemsProvider;
			_activeItemSelector = activeItemSelector;

			Expose();
		}

		private PageViewModel _defaultActiveItem;

		private static readonly string SettingsFolder = Environment.SpecialFolder.ApplicationData.GetPath();
		private static readonly string ActivationTablePath = Path.Combine(SettingsFolder, "ActivationTable");
		private static readonly Dictionary<string, (string Key, string SearchPattern)> FullKeyToActiveItemKey;

		public static event Action<PageViewModel> PageActivated;

		static PageViewModel()
		{
			try
			{
				Directory.CreateDirectory(SettingsFolder);

				if (File.Exists(ActivationTablePath))
				{
					lock (ActivationTablePath)
					{
						var snapshot = File.ReadAllText(ActivationTablePath);
						FullKeyToActiveItemKey = Read(snapshot);
					}
				}
				else FullKeyToActiveItemKey = new();
			}
			catch
			{
			}
		}

		static Dictionary<string, (string Key, string SearchPattern)> Read(string snapshot) => snapshot
			.SplitByChars("\n")
			.Select(l => l.SplitByChars("\t", keepEmptyEntries: true))
			.ToDictionary(p => p[0], p => (p[1], p.Length > 2 ? p[2] : default));

		static string Keep(Dictionary<string, (string Key, string SearchPattern)> table) => table
			.Aggregate("", (s, r) => $"{s}{r.Key}\t{r.Value.Key}\t{r.Value.SearchPattern}\n");

		public void Expose()
		{
			var refresh = Context.Refresh;
			var activate = Context.Get("Activate");

			this[refresh].CanExecute += args => args.CanExecute = IsBusy.Is(false);
			this[refresh].Executed += async args => await Load();
			this[activate].CanExecute += args => args.CanExecute = ActiveItem.IsNot(_defaultActiveItem);
			this[activate].Executed += args => Set(() => ActiveItem, _defaultActiveItem);

			this[() => IsBusy].Changed += args =>	refresh.EvokeCanExecuteChanged();
			this[() => ActiveItem].Changed += args => activate.EvokeCanExecuteChanged();
			this[() => ActiveItem].Changed += args =>
			{
				var activeItem = ActiveItem;
				if (_activeItemSelector.Is()) return;
				FullKeyToActiveItemKey[FullKey] = (activeItem?.Key, SearchPattern);
				PageActivated?.Invoke(ActiveItem);
			};

			this[() => ActiveItem].Changed += async args =>
			{
				var activeItem = ActiveItem;
				if (activeItem.IsNot()) return;
				var json = ActiveItem.GetJsonFromRoot();
				var (from, till) = (2, json.IndexOf("\":") - 2);
				var source = json.Substring(from, till);
				var value = json.Substring(till + 4, json.Length - till - 5);
				YandexMetrica.ReportJson(source, value);
				await Keep();
			};

			SearchPattern = FullKeyToActiveItemKey.TryGetValue(FullKey, out var value)
				? value.SearchPattern
				: default
				;

			var searchPatternTimestamp = DateTime.Now;
			this[() => SearchPattern].Changed += async args =>
			{
				var originalTimestamp = searchPatternTimestamp = DateTime.Now;
				await Task.Delay(3000);
				if (searchPatternTimestamp.IsNot(originalTimestamp)) return;
				var activeItem = ActiveItem;
				FullKeyToActiveItemKey[FullKey] = (activeItem?.Key, SearchPattern);
				await Keep();
			};
		}

		public async Task Keep()
		{
			var snapshot = Keep(FullKeyToActiveItemKey);
			await Task.Run(() =>
			{
				try
				{
					lock (ActivationTablePath)
					{
						var transactionPath = ActivationTablePath + ".new";
						File.WriteAllText(transactionPath, snapshot);
						File.Delete(ActivationTablePath);
						File.Move(transactionPath, ActivationTablePath);
					}
				}
				catch (Exception exception)
				{
					YandexMetrica.ReportError(exception.Message, exception);
				}
			});
		}

		public async Task<PageViewModel> Activate(string itemKey)
		{
			if (_items.IsNot())
				await Load();

			var items = _items;
			if (items.IsNot())
				return default;

			var activeItem = items.FirstOrDefault(i => i.Key.Is(itemKey));
			if (ActiveItem.IsNot(activeItem))
				ActiveItem = activeItem;

			return activeItem;
		}

		public async Task Load()
		{
			if (_itemsProvider.IsNot())
				return;

			var silentRepeats = 0;

			Repeat:

			ActiveException = default;
			try
			{
				IsBusy = true;
				_items = (await _itemsProvider(this)).ToList();

				EvokePropertyChanged(nameof(Items));

				ActiveItem = _defaultActiveItem = _activeItemSelector.Is()
					? _activeItemSelector(_items)
					: FullKeyToActiveItemKey.TryGetValue(FullKey, out var activeItem)
						? _items.FirstOrDefault(i => i.Key.Is(activeItem.Key))
						: ActiveItem ?? (_items.Count > 5 ? ActiveItem : _items.FirstOrDefault());
			}
			catch (Exception exception)
			{
				ActiveException = exception;
				for (var e = exception; e.Is(); e = e.InnerException)
					YandexMetrica.ReportError(e.Message, e);
				//if (exception.IsNot<WebException>())
				//	YandexMetrica.Flush(); // cause locks
			}
			finally
			{
				IsBusy = false;
			}

			if (silentRepeats.Is(0) && ActiveException.Is(out var activeException) && App.RootView.Is(out var view))
			{
				var accept = "Repeat".Localize();
				var cancel = "Skip".Localize();
				var title = "Error".Localize();
				var message = activeException.Is(out HttpRequestException requestException)
					? $"({requestException.Message})\n\n{"CheckInternetConnectionMessage".Localize()}"
					: activeException.Message;
				var result = await view.DisplayAlert(title, message, accept, cancel);
				if (result.Is(true))
				{
					// try to rewrite cached requests with possible invalid data 
					Extensions.WebSyncer.Default.Mode = Extensions.WebSyncer.Modes.ForceSync;
					goto Repeat;
				}
				else
				{
					// try to reuse cached requests
					Extensions.WebSyncer.Default.Mode = Extensions.WebSyncer.Modes.SilentRepeat;
					silentRepeats++;
					goto Repeat;
				}
			}
		}

		public Exception ActiveException
		{
			get => Get(() => ActiveException);
			set => Set(() => ActiveException, value);
		}

		public bool IsBusy
		{
			get => Get(() => IsBusy);
			set => Set(() => IsBusy, value);
		}

		[DataMember]
		public string Key
		{
			get => Get(() => Key);
			set => Set(() => Key, value);
		}

		public string Value
		{
			get => Get(() => Value);
			set => Set(() => Value, value);
		}

		public PageViewModel Root { get; private set; }

		public string FullKey => Root.Is() ? $"{Root.FullKey}•{Key}" : Key.As("_");

		public string GetJsonFromRoot(bool includeRoot = false)
		{
			try
			{
				var value = "null";
				for (var activeItem = this; activeItem.Is(); activeItem = activeItem.Root)
				{
					if (includeRoot.Is(false) && activeItem.Root.IsNot()) break;
					var isSource = activeItem.Root.Key.Is("_");
					var key = isSource ? activeItem.Key : activeItem.Value;
					if (key.IsNot()) break;
					value = $"{{{key.ToJson()}:{value}}}";
				}
				return value;
			}
			catch (Exception exception)
			{
				YandexMetrica.ReportError(exception.Message, exception);
				return default;
			}
		}

		[DataMember]
		public string SearchPattern
		{
			get => Get(() => SearchPattern);
			set => Set(() => SearchPattern, value);
		}

		[DataMember]
		private IList<PageViewModel> _items;
		public IList<PageViewModel> Items
		{
			get
			{
				if (_items.IsNot() && IsBusy.Is(false))
					Load().RunAsync();

				return _items;
			}
		}

		[DataMember]
		public PageViewModel ActiveItem
		{
			get => Get(() => ActiveItem);
			set => Set(() => ActiveItem, value ?? ActiveItem, true);
		}

		public bool ForceActiveItemRefresh()
		{
			if (IsBusy) return false;
			var activeItem = ActiveItem;
			var activeItemPropertyName = nameof(ActiveItem);
			Set(activeItemPropertyName, default(PageViewModel));
			Set(activeItemPropertyName, activeItem);
			return true;
		}

		public override string ToString() => $"({Key} : {Value})";
	}
}
