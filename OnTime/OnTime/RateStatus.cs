﻿using Ace;
using System;
using System.Globalization;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace OnTime
{
	public static class RateStatus
	{
		public static string AppStoreLink { get; set; }

		public static async Task OpenAppStoreLink()
		{
			try
			{
				await Browser.OpenAsync(AppStoreLink);
			}
			catch (Exception exception)
			{
				Yandex.Metrica.YandexMetrica.ReportError(exception.Message, exception);
			}
		}

		public static async Task<string> UpdateRateStatus(string rateStatus, uint wakeCounter)
		{
			var culture = CultureInfo.InvariantCulture;

			if
			(
				AppStoreLink.Is() &&
				(rateStatus.TryParse(culture, out DateTime timestamp).IsFalse() || (DateTime.Now - timestamp).Days > 69) &&
				wakeCounter > 15 && wakeCounter % 10 is 0
			)
			{
				Yandex.Metrica.YandexMetrica.Report("RateRequest", wakeCounter.ToString());

				await Task.Delay(3000);
				var rateAnswer = await MainThread.InvokeOnMainThreadAsync(async () => await App.RootView.DisplayAlert
				(
					"Question".Localize(),
					"RateQuestion".Localize(),
					"Yes".Localize(),
					"No".Localize()
				));

				Yandex.Metrica.YandexMetrica.Report("RateAnswer", rateStatus);

				if (rateAnswer.IsFalse())
					return rateStatus;

				await OpenAppStoreLink();

				return DateTime.Now.ToString(culture);
			}
			else
				return rateStatus;
		}
	}
}
