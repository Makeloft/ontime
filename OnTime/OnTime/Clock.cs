﻿using System;
using System.Threading.Tasks;

namespace OnTime
{
	class Clock
	{
		public const double ActiveDayOffsetHours = 3.5d;
		public static DateTime GetTimetableDate() => GetActiveDateTime().AddHours(-ActiveDayOffsetHours).Date;
		public static DateTime GetActiveDateTime() => DateTime.Now;

		public Clock() => StartMinuteChime();

		public event Action MinuteChime;

		private async void StartMinuteChime()
		{
			Loop:
			var activeDateTime = GetActiveDateTime();
			var seconds = 60 - activeDateTime.Second;
			await Task.Delay(seconds * 1000);
			MinuteChime?.Invoke();
			goto Loop;
		}
	}
}
