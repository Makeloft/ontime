﻿using Ace;
using Ace.Controls;
using Ace.Markup.Patterns;

using System;
using System.Linq;
using System.Net;

using Xamarin.Forms;

using OnTime.Extensions;

namespace OnTime
{
	public partial class AppView
	{
		public AppView() => InitializeComponent();

		public View ActiveOverlay => Overlay.Children.LastOrDefault();

		public void ShowOverlay(View view)
		{
			Overlay.IsVisible = true;
			Overlay.Children.Add(view);
		}

		public void HideOverlay(View view)
		{
			Overlay.Children.Remove(view);
			Overlay.IsVisible = Overlay.Children.Count > 0;
		}

		public Rack GetGhostRack() => GhostRack;

		private async void Button_Clicked(object sender, EventArgs e)
		{
			try
			{
				BindingContext.To<View>().IsVisible = false;
				BindingContext = sender.To<Button>().CommandParameter;
				BindingContext.To<View>().IsVisible = true;

				Yandex.Metrica.YandexMetrica.Report("Navigate", BindingContext.To<string>());
			}
			catch (Exception activeException)
			{
				if (App.RootView.Is(out var view))
				{
					var cancel = "Skip".Localize();
					var title = "Error".Localize();
					var message = activeException is WebException webException
						? $"({webException.Status})\n\n{"CheckInternetConnectionMessage".Localize()}"
						: activeException.Message;
					await view.DisplayAlert(title, message, cancel);
				}
			}
		}

		private object ViewToFontSize(ConvertArgs args) => args.Value.Is(args.Parameter)
			? App.Current.Resources.GetValue("ActiveTabFontSize", 22.0001d)
			: App.Current.Resources.GetValue("TabFontSize", 16.0001d);

		private void Label_SizeChanged(object sender, EventArgs e) => this.ResetValue(BindingContextProperty);
	}
}