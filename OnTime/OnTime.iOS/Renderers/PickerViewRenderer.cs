﻿using Ace;

using OnTime.iOS.Renderers;

using UIKit;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using ObjCRuntime;

[assembly: ExportRenderer(typeof(OnTime.Views.Picker), typeof(PickerViewRenderer))]

namespace OnTime.iOS.Renderers
{
	public class PickerViewRenderer : PickerRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
		{
			base.OnElementChanged(e);

			if (e.NewElement.IsNot())
				return;

			Element.To(out Views.Picker picker);

			var field = new ReadOnlyField
			{
				ClipsToBounds = true,
				InputView = new UIView(), // don't show a screen keyboard
				BorderStyle = UITextBorderStyle.RoundedRect, // or see field.Layer
				Font = UIFont.SystemFontOfSize((float)Element.FontSize),
				TextColor = picker.TextColor.ToUIColor(),
				Text = picker.SelectedValue.ToStr() // force the color to update
			};

			field.Started += (o, ex) =>
			{
				field.EndEditing(true);
				picker.OnClick();
			};

			SetNativeControl(field);
		}

		internal class ReadOnlyField : UITextField
		{
			public ReadOnlyField() : base(new CoreGraphics.CGRect())
			{
				SpellCheckingType = UITextSpellCheckingType.No;
				AutocorrectionType = UITextAutocorrectionType.No;
				AutocapitalizationType = UITextAutocapitalizationType.None;
				ShouldChangeCharacters = new UITextFieldChange((f, r, s) => false);
			}

			//static readonly string[] enableActions = { "copy:", "select:", "selectAll:" };

			public override bool CanPerform(Selector action, NSObject withSender) => false; // enableActions.Contains(action.Name);
		}
	}
}