﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ace;
using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	public class Gomeltrans : AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex DirectionName, Regex Hour, Regex Minute) CompileRegexes() =>
		(
			/*lang=regex*/@"<a href=./(.+?)/(.+?)/(?<key>.+?)/.+?><b>(?<value>.+?)</b>(.+?)</a>".ToRegex(),
			/*lang=regex*/@"<div class=.t-elem.>[\s\S]*?</table>\s+?</div>(\s*?<div[\s\S]+?</div>)*".ToRegex(),
			/*lang=regex*/"<a href=./routes/(.+?)/(.+?)/(?<key>.+?)/.>(?<value>.*?)</a>".ToRegex(),
			/*lang=regex*/@"<div class=.schedule-graphic.>[\s\S]*?<h2 class=.+?>(?<key>.+?)</h2>\s*<div[\s\S]+?\s+</div>".ToRegex(),
			/*lang=regex*/@"<td class=.route-stop.*.>(?<name>.*)".ToRegex(),
			/*lang=regex*/@"<div class=.sch-hour.>\s*<div class=.sch-h.*?.>(?<key>\d+?)</div>(?<value>[\s\S]+?</div>)\s*</div>\s*</div>".ToRegex(),
			/*lang=regex*/@">(?<value>\d+)</div>".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex DirectionName, Regex Hour, Regex Minute) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex DirectionName, Regex Hour, Regex Minute) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => kind switch
		{
			Kinds.Bus => "bus",
			Kinds.Trolleybus => "trolleybus",
			_ => throw new NotImplementedException()
		};

		public override string GetRouteNumbersSource() => $"{Host}/routes/{Kind}/";

		protected override async Task<IEnumerable<Context>> GetRoutes(Regex regex)
		{
			var page = await GetRouteNumbersSource().Load(false, Windows1251.Encoding);
			return regex.Matches(page).Cast<Match>().Select(m => new Context
			(
				Uri.UnescapeDataString(m.Groups["key"].Value),
				Uri.UnescapeDataString(m.Groups["value"].Value),
				m
			) {SourcePage = page});
		}

		public override async Task<IEnumerable<Context>> GetRoutes() => await GetRoutes(Regexes.Route);

		public override async Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var page = await $"{Host}/routes/{Kind}/{route.Key}/".Load(false, Windows1251.Encoding);
			var directions = Regexes.Direction.Matches(page.Replace("&nbsp;", " ")).Cast<Match>().ToList();

			return directions.Select(m =>
			{
				var key = Regexes.DirectionName.Matches(m.Value).Cast<Match>()
					.Aggregate(default(string), (s, match) => s.Is()
						? $"{s} - {match.Groups["name"].Value.Trim()}"
						: match.Groups["name"].Value.Trim());
				return new Context(key, key, m);
			});
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction) =>
			await Regexes.Stop.Matches(direction.Match.Value).Cast<Match>().Select(ToContext).ToAsync();

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>> GetTimetables(Context route, Context direction, Context stop)
		{
			var page = await $"{Host}/routes/{Kind}/{route.Key}/{stop.Key}/".Load(false, Windows1251.Encoding);
			return Regexes.Timetable.Matches(page.Replace("&nbsp;", " ")).Cast<Match>().Select(m =>
			{
				GetRowsAsync getRows = async () => await Regexes.Hour.Matches(m.Value).Cast<Match>().Select(h =>
				{
					var minutes = Regexes.Minute.Matches(h.Groups["value"].Value).Cast<Match>().ToString(m => m.Groups["value"].Value);
					return (h.Groups["key"].Value, minutes);
				}).ToAsync();
				return (FixPeriod(m.Groups["key"].Value), getRows);
			});
		}
	}
}
