﻿using Ace;

using OnTime.Parsers.Patterns;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OnTime.Parsers
{
	public class Kyivpastrans : AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Rows, Regex Cell) CompileRegexes() =>
		(
			/*lang=regex*/@"<a.class=.btn btn-default transport-link. href=./(.+?)/(.+?)/(.+?)/.+?>(?<key>.+?)</a>".ToRegex(),
			/*lang=regex*/@"<div class=.col-xs-12 col-sm-6.>[\s\S]*?<span class=.name-route.>(?<key>.+?)</span>[\s\S]*?</tbody>".ToRegex(),
			/*lang=regex*/"<div>(?<key>.+?)</div></th>".ToRegex(),
			/*lang=regex*/@"<tr[\s\S]+?</tr>".ToRegex(),
			/*lang=regex*/@"<td.*?>(?<hour>\d+?):(?<minute>.+?)</td>".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Rows, Regex Cell) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Rows, Regex Cell) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => kind switch
		{
			Kinds.Bus => "bus",
			Kinds.Tram => "tram",
			Kinds.Trolleybus => "trolley",
			_ => throw new NotImplementedException()
		};

		public override string GetRouteNumbersSource() => $"{Host}/schedule/{Kind}/weekday";

		public override Task<IEnumerable<Context>> GetRoutes() => GetRoutes(Regexes.Route);

		public override async Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var workdayPage = await $"{Host}/schedule/{Kind}/{route.Key}/weekday".Load();
			var weekendPage = await $"{Host}/schedule/{Kind}/{route.Key}/weekend".Load();
			var workdayDirections = Regexes.Direction.Matches(workdayPage).Cast<Match>().Select(ToContext);
			var weekendDirections = Regexes.Direction.Matches(weekendPage).Cast<Match>().Select(ToContext);

			return workdayDirections.Union(weekendDirections, Context.DefaultKeyComparer);
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction)
		{
			var page = await direction.Match.Value.ToAsync();
			var stops = Regexes.Stop.Matches(page).Cast<Match>().Skip(1).Select(ToContext).ToList();
			direction.SourceData = stops;
			return stops;
		}

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>> GetTimetables(Context route, Context direction, Context stop)
		{
			async Task<IEnumerable<(string Hour, string Minutes)>> GetRows(string period)
			{
				var page = await $"{Host}/schedule/{Kind}/{route.Key}/{period}".Load();
				var directions = Regexes.Direction.Matches(page).Cast<Match>().Select(ToContext);
				var activeDirection = directions.FirstOrDefault(d => d.Key.Is(direction.Key));
				var stops = direction.SourceData.To<IList<Context>>();
				var stopOffset = stops.OffsetOf(c => c.Key.Is(stop.Key));
				return GetTimetable(Regexes.Rows, activeDirection.Is() ? activeDirection.Match.Value : "", stopOffset);
			};

			return await new (string, GetRowsAsync)[]
			{ 
				("Workdays", async () => await GetRows("weekday")),
				("Weekends", async () => await GetRows("weekend")),
			}.ToAsync();
		}

		public IEnumerable<(string Hour, string Minutes)> GetTimetable(Regex regex, string page, int stopOffset) => regex.Matches(page)
			.Cast<Match>()
			.Skip(1)
			.SelectMany(row => Regexes.Cell.Matches(row.Value).Cast<Match>().Skip(stopOffset).Take(1))
			.GroupBy(m => m.Groups["hour"].Value)
			.Select(g => (g.Key, g.ToString(m => m.Groups["minute"].Value)))
			;
	}
}
