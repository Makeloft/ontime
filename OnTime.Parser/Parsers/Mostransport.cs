﻿using Ace;

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using OnTime.Extensions;
using OnTime.Parsers.Patterns;

using static OnTime.Parsers.Patterns.Kinds;
using System.Net.Http;

namespace OnTime.Parsers
{
	partial class Mostransport : AParser
	{
		static (Regex Stop, Regex Hour, Regex Minute) CompileRegexes() =>
		(
			/*lang=regex*/@"<li [\s\S]+?data-stop=.(?<key>.+?).[\s\S]+?<div class=.a_dotted d-inline.\s*?>(?<value>.+?)</div>[\s\S]+?</li>".ToRegex(),
			/*lang=regex*/@"<div class=.dt1.><strong>(?<key>\d+).*?</strong>(?<value>[\s\S]+?</div>[\s]+?</div>[\s]+?</div>[\s]+?</div>[\s]+?)".ToRegex(),
			/*lang=regex*/@">[\s]*?(?<value>\d+)[\s]*?<".ToRegex()
		);

		static (Regex Stop, Regex Hour, Regex Minute) regexes;
		static (Regex Stop, Regex Hour, Regex Minute) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => kind switch
		{
			Bus => "avto",
			Tram => "tram",
			Trolleybus => "trol",
			_ => throw new NotImplementedException()
		};

		public static string ReadRoutesPage(string path, Kinds kind)
		{
			using var stream = new FileStream(path, FileMode.OpenOrCreate);
			using var archive = new ZipArchive(stream, ZipArchiveMode.Update);
			var name = $"{kind}.txt";
			var entry = archive.GetEntry(name);
			using var reader = new StreamReader(entry.Open());
			return reader.ReadToEnd();
		}

		public override async Task<IEnumerable<Context>> GetRoutes()
		{
			var path = await FileSyncer.Default.GetPath("transport.mos.ru.zip");
			var page = await Task.Run(() => ReadRoutesPage(path, OriginalKind));

			return page
				.SplitByChars("\n")
				.Select(l => l.SplitByChars("\t\n"))
				.Select(v => new Context(v[1], v[0]) { SourceData = v });
		}

		public override async Task<IEnumerable<Context>> GetDirections(Context route) =>
			await route.SourceData.To<string[]>()[2].SplitByChars("|\r")
				.Let(out int i)
				.Select(v => new Context((i++ % 2).ToString(), v)).ToAsync();

		public static Dictionary<string, string> CustomHeaders { get; } = new()
		{
			//["Accept-Encoding"] = "deflate",
			["X-Requested-With"] = "XMLHttpRequest",
			["User-Agent"] = "Mozilla/5.0 (Linux; Android 5.0)",
		};

		private HttpContext _context;
		private HttpContext GetHttpContext()
		{
			if (_context.Is())
				return _context;

			var handler = Aides.CreateCustomHttpHandler();
			var client = new HttpClient(handler);
			Aides.CustomHeaders.ForEach(client.DefaultRequestHeaders.Add);
			CustomHeaders.ForEach(client.DefaultRequestHeaders.Add);

			return _context = new HttpContext
			{
				Client = client,
			};
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction)
		{
			var context = GetHttpContext();
			var date = GetOffsetDateString(3);
			var page = await $"{Host}/ru/ajax/App/ScheduleController/getRoute?mgt_schedule[date]={date}&mgt_schedule[route]={route.Key}&mgt_schedule[direction]={direction.Key}".Load(context);
			return Regexes.Stop.Matches(page).Cast<Match>().Select(m => new Context(m.Groups["key"].Value, m.Groups["value"].Value, m));
		}

		static int GetOffsetDays(DateTime today, int from) => (from - (int)today.DayOfWeek).To(out var offset) < 0 ? offset + 7 : offset;
		static DateTime GetOffsetDate(DateTime today, int from) => today.AddDays(GetOffsetDays(today, from));
		static string GetOffsetDateString(int from) => GetOffsetDate(Clock.GetTimetableDate(), from).ToString("dd.MM.yyyy");

		readonly int[] weekRange = Enumerable.Range(1, 7).ToArray();

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>>
			GetTimetables(Context route, Context direction, Context stop) =>
			await weekRange.Select(i =>
			{
				GetRowsAsync getRows = async () =>
				{
					var context = GetHttpContext();
					var date = GetOffsetDateString(i % 7);
					var page = await $"{Host}/ru/ajax/App/ScheduleController/getRoute?mgt_schedule[date]={date}&mgt_schedule[route]={route.Key}&mgt_schedule[direction]={direction.Key}".Load(context);
					var stops = Regexes.Stop.Matches(page).Cast<Match>().Select(m => new Context(m.Groups["key"].Value, m.Groups["value"].Value, m));
					var activeStop = stops.FirstOrDefault(s => s.Key.Is(stop.Key));
					if (activeStop.IsNot()) return Enumerable.Empty<(string, string)>();

					var hourLines = Regexes.Hour.Matches(activeStop.Match.Value).Cast<Match>();

					return hourLines
						.Select(h =>
						(
							Key: h.Groups["key"].Value,
							Value: Regexes.Minute.Matches(h.Groups["value"].Value).Cast<Match>().ToString(m => m.Groups["value"].Value))
						);
				};
				return (Key: NumToDay(i).ToString(), GetRows: getRows);
			}).ToAsync();

		protected static DayOfWeek NumToDay(int i) => (DayOfWeek)(i % 7);
	}
}
