﻿using Ace;
using Ace.Replication.Models;
using Ace.Serialization;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	class Minsktrans : Basetrans
	{
		public Minsktrans() : base()
		{
			Root = "lookout_yard";
			Part = "minsk";
		}
	}

	class Gusts : Basetrans
	{
		public Gusts() : base()
		{
			Root = "routetaxi";
			Part = "vsemarshruty";
		}

		protected override string FixRouteName(string name) => name.EndsWith("ТК")
			? name.Substring(0, name.Length - 2)
			: name
			;
	}

	class Basetrans : AParser
	{
		protected override string GetKind(Kinds kind) => kind switch
		{
			Kinds.Bus => "bus",
			Kinds.Tram => "tram",
			Kinds.Minibus => "routetaxi",
			Kinds.Trolleybus => "trolleybus",
			_ => throw new NotImplementedException()
		};

		public Basetrans()
		{
#if !PARSER
			SetLanguage(Store.Get<AppViewModel>().To(out var vm));
			vm[() => vm.ActiveLanguage].Changed += args => SetLanguage(vm);
#endif
		}
#if !PARSER
		private void SetLanguage(AppViewModel vm)
		{
			_context?.Client.Dispose();
			_context = default;
			_landuageCode =
				vm.ActiveLanguage.Is(LanguageCodes.Russian) ? "ru" :
				vm.ActiveLanguage.Is(LanguageCodes.Belorussian) ? "be" :
				vm.ActiveLanguage.Is(LanguageCodes.English) && vm.UseTransliteration ? "en" :
				AppViewModel.GetDefaultLanguage().Is(LanguageCodes.Russian) ? "ru" :
				"be";
		}
#endif

		protected Regex TokenRegex = /*lang=regex*/"<input name=.__RequestVerificationToken. type=.hidden. value=\"(?<token>.+?)\" />".ToRegex();

		static readonly KeepProfile KeepProfile = new() { EscapeProfile = { AsciMode = true } };

		private HttpContext _context;
		private string _landuageCode = "ru";

		public string Root { get; set; }
		public string Part { get; set; }

		private HttpContext GetHttpContext()
		{
			if (_context.Is())
				return _context;

			var handler = Aides.CreateCustomHttpHandler();
			var client = new HttpClient(handler);
			Aides.CustomHeaders.ForEach(client.DefaultRequestHeaders.Add);
			client.DefaultRequestHeaders.Add("Referer", $"{Host}/{Root}/Home/Index/{Part}#");
			client.DefaultRequestHeaders.Add("Accept-Encoding", "deflate");
			client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Language", _landuageCode);
			string _token = default;

			return _context = new HttpContext
			{
				Client = client, 
				GetBody = async () =>
				{
					var token = _token ?? await GetToken(client);
					return new StringContent($"__RequestVerificationToken={token}", Encoding.UTF8, "application/x-www-form-urlencoded");
				}
			};
		}

		private async Task<string> GetToken(HttpClient client)
		{
			var response = await client.GetAsync($"{Host}/{Root}/Home/Index/{Part}#");
			var content = await response.Content.ReadAsStringAsync();
			var token = TokenRegex.Match(content).Groups["token"].Value;
			return token;
		}

		public override async Task<IEnumerable<Context>> GetRoutes()
		{
			var context = GetHttpContext();
			var data = await $"{Host}/{Root}/Data/RouteList?p={Part}&tt={Kind}".Load(context);
			var routes = data.ParseSnapshot(keepProfile: KeepProfile).MasterState.To<Map>()["Routes"].To<Set>();
			return routes.Cast<Map>().Select(i => new Context((string)i["Number"], FixRouteName((string)i["Number"])));
		}

		protected virtual string FixRouteName(string name) => name;

		public override async Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var context = GetHttpContext();
			var data = await $"{Host}/{Root}/Data/Route?p={Part}&r={route.Key}&tt={Kind}".Load(context);
			var map = data.ParseSnapshot(keepProfile: KeepProfile).MasterState.To<Map>()["Trips"].To<Map>();
			var count = map.Count(i => i.Key.StartsWith("Stops"));
			var keys = Enumerable.Range(0, count).Select(i => ToAlphabetLetter(i).ToString());
			return keys.Where(k => map[$"Stops{k}"].Is()).Select(k => new Context(k, (string)map["Name" + k]) { SourceData = map });
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction)
		{
			var key = direction.Key;
			var map = direction.SourceData.To<Map>();
			var stops = map[$"Stops{key}"].To<Set>();
			var stopNames = map[$"StopNames{key}"].To<Set>();
			for (var i = 0; i < stops.Count; i++) stops[i].To<Map>()["Name"] = stopNames[i];
			return await stops.Cast<Map>().Select(i => new Context(i["Id"].To<string>(), (string)i["Name"])).ToAsync();
		}

		readonly int[] weekRange = Enumerable.Range(1, 7).ToArray();

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>>
			GetTimetables(Context route, Context direction, Context stop) =>
			await weekRange.Select(i =>
			{
				var context = GetHttpContext();
				GetRowsAsync getRows = async () =>
				{
					var dayFlag = 1 << (i - 1);
					var directionKey = FromAlphabetLetter(direction.Key[0]);
					var data = await $"{Host}/{Root}/Data/Schedule?p={Part}&r={route.Key}&s={stop.Key}&d={directionKey}&tt={Kind}".Load(context);
					var set = data.ParseSnapshot(keepProfile: KeepProfile).MasterState.To<Map>()["DaysOfWeek"].To<Set>();
					var timetable = set.Cast<Map>().FirstOrDefault(m => (m["DaysOfWeek"].To<int>() & dayFlag) != 0);

					if (timetable.IsNot())
						return Enumerable.Empty<(string, string)>();

					var hourLines = timetable["HourLines"].To<Set>();
					return hourLines.Cast<Map>().Select(l => (l["Hour"].To<string>(), l["Minutes"].To<string>()));
				};
				return (Key: NumToDay(i).ToString(), GetRows: getRows);
			}).ToAsync();

		protected static char ToAlphabetLetter(int i) => (char)('A' + i);
		protected static int FromAlphabetLetter(char letter) => letter - 'A';

		protected static DayOfWeek NumToDay(int i) => (DayOfWeek)(i % 7);
	}
}
