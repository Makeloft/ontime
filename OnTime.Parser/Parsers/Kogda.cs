﻿using Ace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	public class Kogda : AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Cell) CompileRegexes() =>
		(
			/*lang=regex*/@"<a class=.btn.*?\s*?href=.*/(?<key>.+?).\s*?>\s*(?<value>.+?)\s*</a>".ToRegex(),
			/*lang=regex*/@"<a role=.button. data-toggle=.*? data-parent=.#directions.[\s]*href=.(?<key>.+?).[\s]*aria-expanded=.*? aria-controls=.*?>[\s]*(?<value>.+?)[\s]*</a>[\s\S]*?</ul>".ToRegex(),
			/*lang=regex*/@"<a[\s]*?href=.(?<ref>.+?).[\s]*?title=.*?[\s]*?>\s*(?<key>.+?)\s*</a>".ToRegex(),
			/*lang=regex*/@"(?<key>\d+):(?<value>\d+)".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Cell) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Cell) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => kind switch
		{
			Kinds.Bus => "autobus",
			Kinds.Trolleybus => "trolleybus",
			Kinds.Tram => "tram",
			Kinds.Metro => "metro",
			_ => throw new NotImplementedException()
		};

		public override string GetSourceKey() => $"{LocalityName}~{base.GetSourceKey()}";
		public override string GetRouteNumbersSource() => $"{Host}/routes/{LocalityName}/{Kind}";
		public override async Task<IEnumerable<Context>> GetRoutes() => await GetRoutes(Regexes.Route);

		public override async Task<IEnumerable<Context>> GetDirections(Context c)
		{
			var page = await $"{Host}/routes/{LocalityName}/{Kind}/{c.Key}".Load();
			if (Kinds.Metro.Is(OriginalKind))
				page = page.Replace("ст.м. ", "");

			return Regexes.Direction.Matches(page).Cast<Match>().Select(m =>
				new Context(Uri.UnescapeDataString(m.Groups["value"].Value), Uri.UnescapeDataString(m.Groups["value"].Value), m));
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction) =>
			await Regexes.Stop.Matches(direction.Match.Value).Cast<Match>().Select(ToContext).ToAsync();

		readonly int[] weekRange = Enumerable.Range(0, 7).ToArray();

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>>
			GetTimetables(Context route, Context direction, Context stop)
		{
			var today = Clock.GetTimetableDate();

			return await weekRange.Select(i =>
			{
				var day = today.AddDays(i);
				var dayName = day.DayOfWeek.ToString();

				GetRowsAsync getRows = async () =>
				{
					var page = await $"{Host}/api/getTimetable?city={LocalityName}&transport={Kind}&route={route.Key}&direction={direction.Key}&busStop={stop.Key}&date={day:yyyy-MM-dd}".Load();
					return GetTimetable(Regexes.Cell, page);
				};
				return (Key: dayName, GetRows: getRows);
			}).ToAsync();
		}
	}
}
