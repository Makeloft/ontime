﻿using Ace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	public class TudaSuda : AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Day, Regex Cell) CompileRegexes() =>
		(
			/*lang=regex*/@"<li><a href=./poleznyashki/raspisanie/.+?/.+?/(?<key>.+?).>(?<value>.+?)</a></li>".ToRegex(),
			/*lang=regex*/@"<div>[\s\S]*?<h3 class=.routname.>(?<value>.+?)</h3>[\s\S]*?</div>".ToRegex(),
			/*lang=regex*/@"<li><a href=./poleznyashki/raspisanie/.+?/.+?/.+?/(?<key>.+?).>(?<value>.+?)</a></li>".ToRegex(),
			/*lang=regex*/@"<td>[\s\S]*?<ul>[\s\S]*?</td>".ToRegex(),
			/*lang=regex*/@"(?<key>\d+):(?<value>\d+)".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Day, Regex Cell) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Day, Regex Cell) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => kind switch
		{
			Kinds.Bus => "bus",
			Kinds.Trolleybus => "trol",
			Kinds.Tram => "tram",
			Kinds.Metro => "metro",
			Kinds.Minibus => LocalityName.Is("brest") ? "express" : "marshrutki",
			_ => throw new NotImplementedException()
		};

		public override string GetSourceKey() => $"{LocalityName}~{base.GetSourceKey()}";
		public override string GetRouteNumbersSource() => $"{Host}/poleznyashki/raspisanie/{LocalityName}/{Kind}";
		public override async Task<IEnumerable<Context>> GetRoutes() => await GetRoutes(Regexes.Route);

		public override async Task<IEnumerable<Context>> GetDirections(Context c)
		{
			var page = await $"{Host}/poleznyashki/raspisanie/{LocalityName}/{Kind}/{c.Key}".Load();
			if (Kinds.Metro.Is(OriginalKind))
				page = page.Replace("Станция метро ", "");

			return Regexes.Direction.Matches(page).Cast<Match>().Let(out var i, 0).Select(m =>
				new Context(i.ToString(), Uri.UnescapeDataString(m.Groups["value"].Value), m));
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction) =>
			await Regexes.Stop.Matches(direction.Match.Value).Cast<Match>().Select(ToContext).ToAsync();

		readonly int[] weekRange = Enumerable.Range(0, 7).ToArray();

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>>
			GetTimetables(Context route, Context direction, Context stop)
		{
			var page = await $"{Host}/poleznyashki/raspisanie/{LocalityName}/{Kind}/{route.Key}/{stop.Key}".Load();
			var daytables = Regexes.Day.Matches(page).Cast<Match>().Select(m => m.Value).ToList();

			return await weekRange.Select(i =>
			{
				var dayOfWeek = (DayOfWeek)((i + 1) % 7);
				var dayName = dayOfWeek.ToString();

				GetRowsAsync getRows = async () =>
				{
					var page = daytables[i];
					return await GetTimetable(Regexes.Cell, page).OrderBy(r => r.Hour.TryParse(out int h) ? h : 0).ToAsync();
				};
				return (Key: dayName, GetRows: getRows);
			}).ToAsync();
		}
	}
}
