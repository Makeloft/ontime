﻿using Ace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	public class Murmansktrolleybus : AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Day, Regex Cell) CompileRegexes() =>
		(
			/*lang=regex*/@"<a href=./schedule/.+?/(?<key>.+?)/.><div class=.display-table.><div class=.display-row.><div class=.display-cell.>(?<value>.+?)</div></div></div></a>".ToRegex(),
			/*lang=regex*/@"<div class=.pp.>(?<value>.+?)</div>".ToRegex(),
			/*lang=regex*/@"<div class=.stop\s*.>[\s\S]*?<span class=.a.>(?<key>.+?)</span>[\s\S]*?<div class=""day action print"">".ToRegex(),
			/*lang=regex*/@"<div class=.day-name.*.>[\n\s]*?<span class=.a.>(?<key>.+?)</span>[\s\S]*?[\n\s]*?</div>[\n\s]*?</div>[\n\s]*?</div>".ToRegex(),
			/*lang=regex*/@"(?<key>\d+):(?<value>\d+)".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Day, Regex Cell) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Day, Regex Cell) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => kind switch
		{
			Kinds.Bus => "bus",
			Kinds.Trolleybus => "trolleybus",
			_ => throw new NotImplementedException()
		};

		public override string GetRouteNumbersSource() => $"{Host}/schedule/";
		public override async Task<IEnumerable<Context>> GetRoutes() => (await GetRoutes(Regexes.Route)).Where(r => r.Match.Value.Contains($"schedule/{Kind}"));

		public override async Task<IEnumerable<Context>> GetDirections(Context c)
		{
			var directions = new List<Context>();
			var directionKeys = new[] { "DIRECT", "REVERSE" };

			foreach (var directionKey in directionKeys)
			{
				var page = await $"{Host}/schedule/{Kind}/{c.Key}/?order={directionKey}".Load();
				var directionValue = Regexes.Direction.Match(page).Groups["value"].Value;
				if (directionValue.IsNullOrWhiteSpace())
					continue;

				var direction = new Context(directionKey, directionValue) { SourcePage = page };
				directions.Add(direction);
			}

			return directions;
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction) =>
			await Regexes.Stop.Matches(direction.SourcePage).Cast<Match>().Select(ToContext).ToAsync();

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>>
			GetTimetables(Context route, Context direction, Context stop)
		{
			return await Regexes.Day.Matches(stop.Match.Value).Cast<Match>().Select(m =>
			{
				var period = m.Groups["key"].Value;
				var periodKey = period.Is("Суббота, Воскресенье") ? "Weekends" : FixPeriod(period);

				GetRowsAsync getRows = async () =>
				{
					return await GetTimetable(Regexes.Cell, m.Value).OrderBy(r => r.Hour.TryParse(out int h) ? h : 0).ToAsync();
				};
				return (Key: periodKey, GetRows: getRows);
			}).ToAsync();
		}
	}
}
