﻿using Ace;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OnTime.Parsers
{
	public class Busget : Patterns.AParser
	{
		static (Regex Kind, Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex DirectionName, Regex Hour, Regex Minute) CompileRegexes() =>
		(
			/*lang=regex*/@"<div class=.transport_title transport_title-(?<key>.+?).><h2>(?<value>.+?)</h2></div>[\s\S]+?</ul>".ToRegex(),
			/*lang=regex*/@"<a href=.(?<key>.+?).>[\s\S]*?<span class=.transport_num.>(?<value>.+?)</span>[\s\S]*?(?<title>.+?)[\s\S]*?</a>".ToRegex(),
			/*lang=regex*/@"<button type=.button. id=.(?<key>.+?). route=..+?..+?>[\s\S]+?<span>.+?</span>[\s\S]+?\s+(?<value>.+?)\s+?</button>".ToRegex(),
			/*lang=regex*/@"<a href=./routes/(.+?)/(.+?)/(?<key>.+?)/.>(?<value>.*?)</a>".ToRegex(),
			/*lang=regex*/@"<div class=.schedule-graphic.>[\s\S]*?<h2 class=.+?>(?<key>.+?)</h2>\s*<div[\s\S]+?\s+</div>".ToRegex(),
			/*lang=regex*/@"<td class=.route-stop.*.>(?<name>.*)".ToRegex(),
			/*lang=regex*/@"<div class=.sch-hour.>\s*<div class=.sch-h.*?.>(?<key>\d+?)</div>(?<value>[\s\S]+?</div>)\s*</div>\s*</div>".ToRegex(),
			/*lang=regex*/@">(?<value>\d+)</div>".ToRegex()
		);

		static (Regex Kind, Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex DirectionName, Regex Hour, Regex Minute) regexes;
		static (Regex Kind, Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex DirectionName, Regex Hour, Regex Minute) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected string locality;
		public string Locality => locality ??= LocalityNameToLocalityCode
			.FirstOrDefault(p => p.Name.Is(LocalityName, StringComparison.OrdinalIgnoreCase)).Code;

		public static (string Name, string Code)[] LocalityNameToLocalityCode =
		{
			("Moscow", "moskva"),
		};

		public override string GetSourceKey() => $"{LocalityName}~{base.GetSourceKey()}";
		public override string GetRouteNumbersSource() => $"{Host}/{LocalityName}";

		public async override Task<IEnumerable<Context>> GetRoutes()
		{
			var page = await $"{Host}/moskva".Load();
			var routeData = Regexes.Kind.Matches(page).Cast<Match>().Skip(1).FirstOrDefault().Value;
			var routes = Regexes.Route.Matches(routeData).Cast<Match>().Select(m => new Context(m));
			return routes;
		}

		public async override Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var page = await $"{Host}/{route.Key}".Load();
			var directions = Regexes.Direction.Matches(page).Cast<Match>().Select(m => new Context(m)).ToList();
			if (directions.Count is 0)
			{
				var value = "<h1>№.+? (?<value>.+?)</h1>".ToRegex().Match(page).Groups["value"].Value;
				directions.Add(new("A", value));
			}

			return directions;
		}

		public override Task<IEnumerable<Context>> GetStops(Context route, Context direction)
		{
			throw new NotImplementedException();
		}

		public override Task<IEnumerable<(string Key, GetRowsAsync GetRows)>> GetTimetables(Context route, Context direction, Context stop)
		{
			throw new NotImplementedException();
		}
	}
}
