﻿using System.IO;

namespace Timely
{
	class Linker
	{
		public static void Do(string inputFileName, string outputFileName)
		{
			var lines = File.ReadLines(inputFileName);
			var counter = 0;
			foreach (var line in lines)
			{
				if (line.StartsWith("\t"))
					Write(outputFileName, $"{line.Trim()}={counter}");
				counter++;
			}
		}

		public static void Write(string fileName, params string[] lines) =>
			File.WriteAllLines(fileName, lines);
	}
}
