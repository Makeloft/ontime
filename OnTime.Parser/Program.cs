﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Linq;

using OnTime.Parsers.Patterns;

using Xamarin.Forms.Internals;
using OnTime.Parsers;

namespace Timely
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.OutputEncoding = Encoding.Unicode;

			//Load();
			Mostransport.Preload("https://transport.mos.ru");

			Console.ReadKey();
		}

		public static Dictionary<string, string> CustomHeaders { get; } = new()
		{
			["Accept-Encoding"] = "deflate",
			["X-Requested-With"] = "XMLHttpRequest",
			["User-Agent"] = "Mozilla/5.0 (Linux; Android 5.0)",
		};

		static async void Load()
		{
			var handler = new HttpClientHandler
			{
				AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate
			};

			var client = new HttpClient(handler);
			CustomHeaders.ForEach(client.DefaultRequestHeaders.Add);

			var uri = "https://transport.mos.ru/ru/ajax/App/ScheduleController/" +
				"getRoutesList" +
				//"?mgt_schedule%5Bsearch%5D=" +
				//"&mgt_schedule%5BisNight%5D=" +
				//"&mgt_schedule%5BworkTime%5D=1" +
				"?mgt_schedule direction=0" +
				"&page=24";
			var page = await client.GetStringAsync(uri);
			page = page;
		}

		static async void Test(AParser parser)
		{
			var stopwatch = new Stopwatch();
			stopwatch.Start();

			try
			{
				await parser.Write();
			}
			catch(Exception exception)
			{
				Console.WriteLine(exception.ToString());
			}

			stopwatch.Stop();
			Console.WriteLine(stopwatch.Elapsed);
		}
	}
}
